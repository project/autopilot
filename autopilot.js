


// Build Function
function showFullBuildPath(bid) {
  var builds = Drupal.settings.autopilot.repos;
  $('#repo_full_path_display').empty().append(Drupal.settings.autopilot.build_list[bid]);
  return;
}

// Transfer Section JavaScript
function dbTransferDisableOption(one, two) {
  elOne = document.getElementById(one);
  elTwo = document.getElementById(two);
  // If one is not selected, and two is not selected (not = 0)
  if(elOne.selectedIndex == 0 && elTwo.selectedIndex == 0) {
    elOne.disabled = false;
    elTwo.disabled = false;
  }
  else {
     if(elOne.selectedIndex != 0) {
      elTwo.selectedIndex = 0;
      elTwo.disabled = true;
     }
     else {
      elOne.selectedIndex = 0;
      elOne.disabled = true;
     }
  }
}
