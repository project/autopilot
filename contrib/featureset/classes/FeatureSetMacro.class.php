<?php

class FeatureSetMacro extends FeatureSet {
  
  private $extra_info = array();
  private $macro_code = '';
  private $macro_in_build = false;
  private $macro_domain = '';
  
  /**
   * This returns an array of options that are
   * specific to this target. This is how we
   * extend a target.
   * NOTE: A GENERAL ARRAY IS RETURNED,
   * THIS ARRAY IS ONLY SIMILAR TO THE DRUPAL
   * ONE. IT GETS TRANSLATED INTO A DRUPAL FAPI.
   * 
   * The options should be in the format:
   * array(
   *  'option' => array(
   *    'name' => 'option name',
   *    'type' => 'string or list',
   *    'description' => 'descitpin about it',
   *    'current_value' => 'current value of the options',
   *    'options' => 'an array of options or null if its singe and a text field'
   *   )
   *  )
   */
  public function featureSetOptions() {
    $options = array();
    // Just a text area
    $options['macro_domain'] = array(
      'field_name' => 'macro_domain',
      'name' => 'Macro Domain',
      'type' => 'string',
      'description' => 'Enter the domain that this marco should be run on. Leave this blank to have
      AutoPilot use the Target for your code\'s domain here. This domain will match/follow the rules
      of Drupal\'s sites folders. E.g. if you want this macro run on the sites folder maching all
      subdomains under holiday.domain.com, you should enter holiday.domain.com to match xmas.holiday.domain.com,
      or you can enter the specific subdomain that you wish to match.',
    );
    $options['macro_code'] = array(
      'field_name' => 'macro_code',
      'name' => 'Macro Code',
      'type' => 'string',
      'description' => 'Place the macro code to run here.',
      'current_value' => $this->getMacroCode(),
      'options' => array('textarea' => true),
    );
    
    return $options;
  }
  
  public function save() {
    try {
      parent::save(); // Parent will throw an error
      $try = db_query("UPDATE {ap_featureset} SET fs_extra = '%s'", serialize($this->getExtra()));
      return $try;
    }
    catch(Exception $e) {
      throw $e;
    }
    return;
  }
  
  /**
   * Override the execute method
   * We want to upload the macro module
   * and run the macro code on the target.
   * This should be done on the Code target, we only want to
   * run this on the TargetWeb instance.
   *
   * TODO:
   * Perhaps this should have the ability to be locked down to a certain
   * target to be run on.
   * 
   */
  public function execute(BuildRun $buildRun = null, Target $target = null, $return_cmds = true) {
    if( ($target instanceof TargetWeb) == false) {
      return;
    }
    
    $macro = $this->getMacroCode();
    $template = $this->getScriptTemplate();
    $macro_domain = $this->getMacroDomain();
    if(trim($macro_domain) == '') {
      $macro_domain = $target->getAddress();
    }
    $vars = array('@macro' => str_replace("'", "\\'", $macro),
                  '@macro_hash' => md5(uniqid(rand(), true)),
                  '@macro_domain' => $macro_domain);
    $template = strtr($template, $vars);

    // Instance of TargetWeb, so ok to run
    # If macro not in current build, we need to upload
    if(!$this->macroInBuild()) {
      # Upload macro module to target
      $remote_path = '/tmp/' . $buildRun->getBuildName() . '/macro.zip';
      $local_path = drupal_get_path('module', 'featureset') . '/misc/macro.zip';
      $remote_zip_path = $target->getBuildPath()  . '/' . $buildRun->getBuildName() . '/html/sites/all/modules/macro';
      $target->makePath('/tmp/' . $buildRun->getBuildName());
      $target->copyFileToTarget($local_path, $remote_path);
      $cmd = "unzip -o -d $remote_zip_path $remote_path"; // Unzip to area
      $target->runRemoteCommand($cmd);
    }
    
    // Run Macro
    $temp_path = $buildRun->getTempArea();
    $temp_path = "$temp_path/" . uniqid('fs_macro') . '.php';
    file_put_contents($temp_path, $template);
    $remote_macro_path = $target->getBuildPath() . '/' . $buildRun->getBuildName() . '/html/fs_macros.php';
    $target->copyFileToTarget($temp_path, $remote_macro_path);
    $remote_build_root = $target->getBuildPath()  . '/' . $buildRun->getBuildName() . '/html';
    $target->runRemoteCommand("cd $remote_build_root && php $remote_macro_path");
    
    
    if(!$this->macroInBuild()) {
      # Remove macro module from target
      $target->runRemoteCommand("rm -rf $remote_zip_path $remote_macro_path");
    }
    
    return;
  }
  
  public function macroInBuild() {
    return $this->macro_in_build;
  }
  public function setMacroInBuild($set) {
    $this->macro_in_build = $set;
  }
  
  protected function getFeatureSetDisplayInfo() {
    return array('name' => 'Macro Feature Set');
  }
  public function getMacroCode() {
    return $this->macro_code;
  }
  public function setMacroCode($code) {
    $this->macro_code = $code;
  }
  public function getMacroDomain() {
    return $this->macro_domain;
  }
  public function setMacroDomain($domain) {
    $this->macro_domain = $domain;
  }
  
  public function setOptions($options) {
    // Set the build options defule
    $this->macro_code = $options['macro_code'];
    $this->macro_domain = $options['macro_domain'];
  }
  
  private function getScriptTemplate() {
    $script =<<<EOD
<?php
define('MACRO_HASH', '@macro_hash');
if(\$_GET['key'] != MACRO_HASH && \$_SERVER['HTTP_REFERER'] != '') {
  exit(1);
}
\$_SERVER['HTTP_HOST'] = '@macro_domain';
\$_SERVER['SCRIPT_NAME'] = '/' . \$_SERVER['SCRIPT_NAME']; // Need to add a slash prefix
require_once('./includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// Enable the Macro Module
module_rebuild_cache();
module_list(true,false);
module_enable(array('macro'));

// Run the Macro
macro_import_macro_submit( 'macro_import_macro', array('macro' => '@macro') );

?>    
EOD;
    return $script;
  }
}

?>