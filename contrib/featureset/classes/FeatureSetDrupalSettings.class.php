<?php

class FeatureSetDrupalSettings extends FeatureSet {
  
  private $extra_info = array();
  private $executing = false;
  
  /**
   * This returns an array of options that are
   * specific to this target. This is how we
   * extend a target.
   * NOTE: A GENERAL ARRAY IS RETURNED,
   * THIS ARRAY IS ONLY SIMILAR TO THE DRUPAL
   * ONE. IT GETS TRANSLATED INTO A DRUPAL FAPI.
   * 
   * The options should be in the format:
   * array(
   *  'option' => array(
   *    'name' => 'option name',
   *    'type' => 'string or list',
   *    'description' => 'descitpin about it',
   *    'current_value' => 'current value of the options',
   *    'options' => 'an array of options or null if its singe and a text field'
   *   )
   *  )
   *
   *  Tempalte settings variables are:
   *  @db_url
   *  @db_prefix
   *  @base_url
   *  @cookie_domain
   *  @conf
   *  @custom
   */
  public function featureSetOptions() {
    $options = array();
    // Just a text area
    $options['drupal_domain'] = array(
      'field_name' => 'drupal_domain',
      'name' => 'Domain',
      'type' => 'string',
      'description' => 'Enter the domain that this settings.php will reside over. This is the folder the will be created.',
      'current_value' => $this->getDomain(),
    );
    $options['db_url'] = array(
      'field_name' => 'db_url',
      'name' => 'Database URL',
      'type' => 'string',
      'description' => 'Enter the DB URL to the database',
      'current_value' => $this->getDBURL(),
    );
    $options['db_prefix'] = array(
      'field_name' => 'db_prefix',
      'name' => 'Database Prefix',
      'type' => 'string',
      'description' => 'Enter the a DB Prefix if one is needed; else leave this blank.',
      'current_value' => $this->getDBPrefix(),
    );
    $options['base_url'] = array(
      'field_name' => 'base_url',
      'name' => 'Base URL',
      'type' => 'string',
      'description' => 'Enter the base url to use. It is usually ok to leave this blank.',
      'current_value' => $this->getBaseURL(),
    );
    $options['cookie_domain'] = array(
      'field_name' => 'cookie_domain',
      'name' => 'Cookie Domain',
      'type' => 'string',
      'description' => 'Cookie domain',
      'current_value' => $this->getCookieDomain(),
    );
    $options['conf'] = array(
      'field_name' => 'conf',
      'name' => 'Configuration Array',
      'type' => 'string',
      'description' => 'Enter the configuration array here. ',
      'current_value' => $this->getConf(),
      'options' => array('textarea' => true),
    );
    $options['custom'] = array(
      'field_name' => 'custom',
      'name' => 'Custom Code',
      'type' => 'string',
      'description' => 'Any custom code that needs to be included in the settings.php file.',
      'current_value' => $this->getCustomCode(),
      'options' => array('textarea' => true),
    );

    
    
    return $options;
  }
  
  public function getDomain() {
    return $this->extra_info['drupal_domain'];
  }
  public function getDBURL() {
    if(!$this->executing) {
      return $this->extra_info['db_url'];
    }
    if(trim($this->extra_info['db_url']) != '') {
      $db_url = '$db_url = \'' . $this->extra_info['db_url'] . '\';';
    }
    return $db_url;
  }
  public function getDBPrefix() {
    if(!$this->executing) {
      return $this->extra_info['db_prefix'];
    }
    if(trim($this->extra_info['db_prefix']) != '') {
      $db_prefix = '$db_prefix = \'' . $this->extra_info['db_prefix'] . '\';';
    }
    return $db_prefix;
  }
  public function getBaseURL() {
    if(!$this->executing) {
      return $this->extra_info['base_url'];
    }
    if(trim($this->extra_info['base_url']) != '') {
      $base_url = '$base_url = \'' . $this->extra_info['base_url'] . '\';';
    }
    return $base_url;
  }
  public function getCookieDomain() {
    if(!$this->executing) {
      return $this->extra_info['cookie_domain'];
    }
    if(trim($this->extra_info['cookie_domain']) != '') {
      $cookie_domain = '$cookie_domain = \'' . $this->extra_info['cookie_domain'] . '\';';
    }
    return $cookie_domain;
  }
  public function getConf() {
    return $this->extra_info['conf'];
  }
  public function getCustomCode() {
    return $this->extra_info['custom'];
  }
  
  public function mergeSettingsFile() {
    $opts = array(
      '@db_url' => $this->getDBURL(),
      '@db_prefix' => $this->getDBPrefix(),
      '@base_url' => $this->getBaseURL(),
      '@cookie_domain' => $this->getCookieDomain(),
      '@conf' => $this->getConf(),
      '@misc' => $this->getCustomCode(),
    );
    
    $settings_info = $this->getSettingsTemplate();
    foreach($opts as $k => $v) {
      if($v != '') {
        $settings_info .= "\n$v";
      }
    }
    
    return $settings_info;
  }
  
  public function save() {
    try {
      parent::save(); // Parent will throw an error
      $try = db_query("UPDATE {ap_featureset} SET fs_extra = '%s'", serialize($this->getExtra()));
      return $try;
    }
    catch(Exception $e) {
      throw $e;
    }
    return;
  }
  
  /**
   * Override the execute method
   * We want to take the code and just "eval" it.
   * The "dummy" object we give them will be loaded wit hthe
   * information we want, and we'll execute it through the array here.
   */
  public function execute(BuildRun $buildRun = null, Target $target = null, $return_cmds = true) {
    if( $target instanceof TargetWeb && trim($this->getDomain()) != "") {
      $this->executing = true;
      # Get the template
      $settings_file = $this->mergeSettingsFile();
      // Now lets write this file
      $tmp_settings_file_path = $buildRun->getTempArea() . '/' . $this->getDomain() . '.settings.php';
      file_put_contents($tmp_settings_file_path, $settings_file);
      
      // Push up to the target
      $build_path = $target->getBuildPath() . '/' . $buildRun->getBuildName() . '/html/sites';
      $target->makePath($build_path . '/' . $this->getDomain());
      $target->copyFileToTarget($tmp_settings_file_path, $build_path . '/' . $this->getDomain() . '/settings.php');
      $this->executing = false;
    }
    
    return;
  }
  
  protected function getFeatureSetDisplayInfo() {
    return array('name' => 'Drupal Settings Feature set');
  }
  public function getBuildScriptCode() {
    return $this->build_code;
  }
  public function setBuildScriptCode($code) {
    $this->build_code = $code;
  }
  
  public function setOptions($options) {
    // Set the build options defule
    $this->extra_info = $options;
  }
  

  
  /**
   *
   * Tempalte settings
   * Variables are:
   * @db_url
   * @db_prefix
   * @base_url
   * @cookie_domain
   * @conf
   * @misc
   *
   */
  private function getSettingsTemplate() {
    $template = <<<EOD
<?php
/**
 * 
 * This file was automatically generated by AutoPilot
 * Domain Usage
 * 
 */
/**
 * PHP settings:
 *
 * To see what PHP settings are possible, including whether they can
 * be set at runtime (ie., when ini_set() occurs), read the PHP
 * documentation at http://www.php.net/manual/en/ini.php#ini.list
 * and take a look at the .htaccess file to see which non-runtime
 * settings are used there. Settings defined here should not be
 * duplicated there so as to avoid conflict issues.
 */

ini_set('arg_separator.output',     '&amp;');
ini_set('magic_quotes_runtime',     0);
ini_set('magic_quotes_sybase',      0);
ini_set('session.cache_expire',     200000);
ini_set('session.cache_limiter',    'none');
ini_set('session.cookie_lifetime',  2000000);
ini_set('session.gc_maxlifetime',   200000);
ini_set('session.save_handler',     'user');
ini_set('session.use_only_cookies', 1);
ini_set('session.use_trans_sid',    0);
ini_set('url_rewriter.tags',        '');

EOD;
  return $template;
  }
  
}

?>
