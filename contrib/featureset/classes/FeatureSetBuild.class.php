<?php

class FeatureSetBuild extends FeatureSet {
  
  private $extra_info = array();
  
  /**
   * This returns an array of options that are
   * specific to this target. This is how we
   * extend a target.
   * NOTE: A GENERAL ARRAY IS RETURNED,
   * THIS ARRAY IS ONLY SIMILAR TO THE DRUPAL
   * ONE. IT GETS TRANSLATED INTO A DRUPAL FAPI.
   * 
   * The options should be in the format:
   * array(
   *  'option' => array(
   *    'name' => 'option name',
   *    'type' => 'string or list',
   *    'description' => 'descitpin about it',
   *    'current_value' => 'current value of the options',
   *    'options' => 'an array of options or null if its singe and a text field'
   *   )
   *  )
   */
  public function featureSetOptions() {
    $options = array();
    // Just a text area
    $options['build_code'] = array(
      'field_name' => 'build_code',
      'name' => 'Build Code',
      'type' => 'string',
      'description' => 'Place your build-code script here. Click here for more information about the Autopilot build-code scripting capabilities.',
      'current_value' => $this->getBuildScriptCode(),
      'options' => array('textarea' => true),
    );
    
    return $options;
  }
  
  public function save() {
    try {
      parent::save(); // Parent will throw an error
      $try = db_query("UPDATE {ap_featureset} SET fs_extra = '%s'", serialize($this->getExtra()));
      return $try;
    }
    catch(Exception $e) {
      throw $e;
    }
    return;
  }
  
  /**
   * Override the execute method
   * We want to take the code and just "eval" it.
   * The "dummy" object we give them will be loaded wit hthe
   * information we want, and we'll execute it through the array here.
   */
  public function execute(BuildRun $buildRun = null, Target $target = null, $return_cmds = true) {
    if(trim($this->getBuildScriptCode()) == '') {
      return;
    }
    
    $SanboxBuild = new stdClass();
    $CurrentBuildRun = new stdClass();
    $CurrentBuildRun->execute = array(); // Initialize the var to avoid errors
    
    $CurrentBuildRun->build_path = $this->remote_build_path;
    eval($this->getBuildScriptCode());
    // Now, lets loop through and checkout what we need to
    
    // Now, go through the mock_build and execute as we need to
    $fs_cmds = array();
    foreach($CurrentBuildRun->execute as $k => $pbuild) {
      
      // Execute the builds
      $builds = $pbuild->getCheckouts();
      $build = Build::buildFactory($pbuild->getID());
      foreach($builds as $k => $build_info) {
        // Checkout command help
        $fs_cmds[] = $build->checkout($build_info['remote_path'], true, true, $build_info['repo_path']);
      }
      
    }
    // Run these commands remotley
    if($return_cmds) {
      return $fs_cmds;
    }
    
  }
  
  protected function getFeatureSetDisplayInfo() {
    return array('name' => 'Build Feature set');
  }
  public function getBuildScriptCode() {
    return $this->build_code;
  }
  public function setBuildScriptCode($code) {
    $this->build_code = $code;
  }
  
  public function setOptions($options) {
    // Set the build options defule
    $this->build_code = $options['build_code'];
  }
}

/**
 * This class is a "DUMMY" class that gets executed by
 * the Build Feature Set
 * PBuild means PrototypeBuild
 *
 * The following variables are availiable
 * AP_BUILD_PATH
 *
 * This works on BUILDS
 * Perhaps later we will make a similar one that works with the
 * repository itself as a base for more power and flexibility.
 * 
 * 
 */
class PrototypeBuild {
  
  private $bid = 0;
  private $checkouts = array();
  private $keyChain = null;
  public function __construct($bid)  {
    $this->bid = $bid;
  }
  
  /**
   * Checks out the code from one of a users other builds
   * to this build.
   */
  public function checkout($repo_path, $remote_path, $export = true, $remote = true) {
    $this->checkouts[] = array('repo_path' => $repo_path, 'remote_path' => $remote_path);
  }
  
  public function setKeyChain($kcid) {
    $this->keyChain = new KeyChain($kcid);
  }
  
  public function getCheckouts() {
    return $this->checkouts;
  }
  
  public function getID() {
    return $this->bid;
  }
  
}

?>
