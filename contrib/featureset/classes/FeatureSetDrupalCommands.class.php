<?php

class FeatureSetDrupalCommands extends FeatureSet {
  
  private $extra_info = array();
  private $drupal_commands = '';
  
  /**
   * This returns an array of options that are
   * specific to this target. This is how we
   * extend a target.
   * NOTE: A GENERAL ARRAY IS RETURNED,
   * THIS ARRAY IS ONLY SIMILAR TO THE DRUPAL
   * ONE. IT GETS TRANSLATED INTO A DRUPAL FAPI.
   * 
   * The options should be in the format:
   * array(
   *  'option' => array(
   *    'name' => 'option name',
   *    'type' => 'string or list',
   *    'description' => 'descitpin about it',
   *    'current_value' => 'current value of the options',
   *    'options' => 'an array of options or null if its singe and a text field'
   *   )
   *  )
   */
  public function featureSetOptions() {
    $options = array();
    // Just a text area
    $options['drupal_domain'] = array(
      'field_name' => 'drupal_domain',
      'name' => 'Drupal Domain',
      'type' => 'string',
      'description' => 'Enter the domain that these drupal commands should be run on. Leave this blank to have
      AutoPilot use the Target for your code\'s domain here. This domain will match/follow the rules
      of Drupal\'s sites folders. E.g. if you want this drupal script to run on the sites folder maching all
      subdomains under holiday.domain.com, you should enter holiday.domain.com to match xmas.holiday.domain.com,
      or you can enter the specific subdomain that you wish to match.',
    );
    $options['drupal_code'] = array(
      'field_name' => 'drupal_code',
      'name' => 'Drupal Code',
      'type' => 'string',
      'description' => 'Place the drupal code/script to run here.',
      'current_value' => $this->getDrupalCode(),
      'options' => array('textarea' => true),
    );
    
    return $options;
  }
  
  public function save() {
    try {
      parent::save(); // Parent will throw an error
      $try = db_query("UPDATE {ap_featureset} SET fs_extra = '%s'", serialize($this->getExtra()));
      return $try;
    }
    catch(Exception $e) {
      throw $e;
    }
    return;
  }
  
  /**
   * Override the execute method
   * Run the drupal script on the target.
   * This should be done on the Code target, we only want to
   * run this on the TargetWeb instance.
   *
   * TODO:
   * Perhaps this should have the ability to be locked down to a certain
   * target to be run on.
   * 
   */
  public function execute(BuildRun $buildRun = null, Target $target = null, $return_cmds = true) {
    if( ($target instanceof TargetWeb) == false) {
      return;
    }
    
    $template = $this->getScriptTemplate();
    $drupal_domain = $this->getDrupalDomain();
    if(trim($drupal_domain) == '') {
      $drupal_domain = $target->getAddress();
    }
    $vars = array('@drupal_code' => $this->getDrupalCode(),
                  '@drupal_hash' => md5(uniqid(rand(), true)),
                  '@drupal_domain' => $drupal_domain);
    $template = strtr($template, $vars);

    // Instance of TargetWeb, so ok to run
    // Run Macro
    $temp_path = $buildRun->getTempArea();
    $temp_path = "$temp_path/" . uniqid('fs_drupal') . '.php';
    $remote_file_name = uniqid('fs_drupal') . '.php';
    file_put_contents($temp_path, $template);
    $remote_file_path = $target->getBuildPath() . '/' . $buildRun->getBuildName() . '/html/' . $remote_file_name;
    $target->copyFileToTarget($temp_path, $remote_file_path);
    $remote_build_root = $target->getBuildPath()  . '/' . $buildRun->getBuildName() . '/html';
    $target->runRemoteCommand("cd $remote_build_root && php $remote_file_path");
    # Remove file from the target
    $target->runRemoteCommand("rm -rf $remote_file_path");    

    
    return;
  }
  
  
  protected function getFeatureSetDisplayInfo() {
    return array('name' => 'Drupal Commands/Script Feature Set');
  }
  public function getDrupalCode() {
    return $this->drupal_code;
  }
  public function setDrupalCode($code) {
    $this->drupal_code = $code;
  }
  public function getDrupalDomain() {
    return $this->drupal_domain;
  }
  public function setDrupalDomain($domain) {
    $this->drupal_domain = $domain;
  }
  
  public function setOptions($options) {
    // Set the build options defule
    $this->drupal_code = $options['drupal_code'];
    $this->drupal_domain = $options['drupal_domain'];
  }
  
  private function getScriptTemplate() {
    $script =<<<EOD
<?php
define('DRUPAL_HASH', '@drupal_hash');
if(\$_GET['key'] != DRUPAL_HASH && \$_SERVER['HTTP_REFERER'] != '') {
  exit(1);
}
\$_SERVER['HTTP_HOST'] = '@drupal_domain';
\$_SERVER['SCRIPT_NAME'] = '/' . \$_SERVER['SCRIPT_NAME']; // Need to add a slash prefix
require_once('./includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// Rebuild the module list to rebuild paths
module_rebuild_cache();
module_list(true,false);

// Run the Drupal Script
@drupal_code


?>    
EOD;
    return $script;
  }
}

?>
