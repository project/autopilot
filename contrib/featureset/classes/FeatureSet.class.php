<?php

class FeatureSet {
  
  private $build_code;
  private $createNew = false;
  private $fsid = null;
  private $name = '';
  private $description = '';
  private $widget_options = array();
  private $autopilot = null;
  private $buildPlan = null;
  
  protected $remote_build_path = '';
  
  public function __construct($fsid = null) {
    // Load up the FeatureSet
    $this->autopilot = AutoPilot::setCurrentAutoPilot();
    if($fsid !== null && is_numeric($fsid)) {
      $this->fsid = $fsid;
      $this->load();
    }
  }
  
  private function load() {
    // Get the info
    $info = db_fetch_object(db_query("SELECT * FROM {ap_featureset} WHERE fsid = %d", $this->getID()));
    
    # Now set the info
    $this->name = $info->fs_name;
    $this->description = $info->fs_desc;
    $this->widget_options = @unserialize($info->fs_extra) or array();
  }
  
  /**
   * Static functions
   * TODO: I would rather find the itmes that implement the FeatureSetWidget
   * interface. However, PHP doesn't currently easily allow that.
   */
  public static function allFeatureSetOptions(FeatureSet $cfs = null) {
    if($cfs != null) {
      $widget_options = $cfs->getWidgetOptions();
    }
    else {
      $widget_options = array();
    }
    
    $classes = get_declared_classes();
    foreach($classes as $k => $classname) {
      if(is_subclass_of($classname, 'FeatureSet'))  {
        $fs = null;
        $fs = new $classname();
        $fs->setOptions($widget_options[$classname]);
        $info = $fs->getFeatureSetDisplayInfo();
        $options[$classname] = $fs->featureSetOptions();
      }
    }
    
    return $options;
  }
  
  public function execute(BuildRun $buildRun = null, Target $target = null, $return_cmds = true) {
    
    // Get all the featureset class names
    $gen_cl = FeatureSet::allFeatureSetOptions();
    $fs_cmds = array();
    
    foreach($gen_cl as $classname => $opt) {
      $tfs = null;
      $tfs = new $classname($this->fsid);
      $tfs->setRemoteBuildPath($this->remote_build_path);
      $tfs->setOptions($this->widget_options[$classname]);
      $cmds = $tfs->execute($buildRun, $target, $return_cmds);
      if(is_array($cmds)) {
        $fs_cmds += $cmds;
      }
    }
    
    return $fs_cmds;
    
  }
  
  public function setRemoteBuildPath($path) {
    $this->remote_build_path = $path;
  }
  
  // Gets featuer set options for
  // instantianted options.
  public function getWidgetOptions() {
    return $this->widget_options;
  }
  
  /**
   * Set the options for the other
   * feature sets that are extended.
   */
  public function setOptions($options) {
    # The array is indexed by the ClassName that
    # the option is for. We only use this when we
    # execute. For now, just set the array,
    # and we'll save it as 'Extra'
    $this->widget_options = $options;
    return;
  }
  
  
  /**
   * Saves/Updates the current
   * feature set.
   */
  public function save() {
    $try = true;
    global $user;
    
    if($this->createNew) {
      $fsid = db_next_id('{ap_featureset}_fsid');
      $this->fsid = $fsid;
      $try = db_query("INSERT INTO {ap_featureset} (fsid, apid, uid, fs_name, fs_desc)
                    VALUES(%d, %d, %d, '%s', '%s')", $fsid, $this->autopilot->getID(), $user->uid, $this->getName(), $this->getDescription());
      if(!$try) {
        db_query("DELETE FROM {ap_featureset} WHERE fsid = %d", $fsid);
        $try = false;
      }
    }
    
    // TODO: Perhaps throw an error here instead?
    if($try == false) {
      return false;
    }
    
    // Update with the other Genral Information
    
    $try = db_query("UPDATE {ap_featureset}
             SET fs_name = '%s', fs_desc = '%s',
             fs_extra = '%s'
             WHERE fsid = %d",
             $this->getName(), $this->getDescription(),
             serialize($this->widget_options), $this->getID());
    return $try;
  }
  
  public function createNew() {
    $this->createNew = true;
  }
  public function getID() {
    return $this->fsid;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
  }
  public function getDescription() {
    return $this->description;
  }
  public function setDescription($desc) {
    $this->description = $desc;
  }
  
  /**
   * Implemenats the SQL feature set
   */
  public function setSQLFeature($sql_code) {
    $this->build_code = $sql_code;
  }
  
  public function featureSetOptions() {
    return array();
  }
  
  /**
   * Gets the SQL Feature
   */
  public function getSQLFeature() {
    return $this->build_code;
  }
  
  /**
   * Build code
   */
  public function setBuildFeature($build_code) {
    $this->build_code = $build_code;
  }
  public function getBuildFeature() {
    return $this->build_code;
  }
  
  
  /**
   * See if this is set for a buildplan or return all BP's its run for
   */
  public function runOnBuildPlan($bpid = null) {
    return false;
  }
}

?>
