<?php

class FeatureSetSQL extends FeatureSet {
  
  private $extra_info = array();
  private $sql_code = '';
  private $drupal_domain = '';
  
  /**
   * This returns an array of options that are
   * specific to this target. This is how we
   * extend a target.
   * NOTE: A GENERAL ARRAY IS RETURNED,
   * THIS ARRAY IS ONLY SIMILAR TO THE DRUPAL
   * ONE. IT GETS TRANSLATED INTO A DRUPAL FAPI.
   * 
   * The options should be in the format:
   * array(
   *  'option' => array(
   *    'name' => 'option name',
   *    'type' => 'string or list',
   *    'description' => 'descitpin about it',
   *    'current_value' => 'current value of the options',
   *    'options' => 'an array of options or null if its singe and a text field'
   *   )
   *  )
   */
  public function featureSetOptions() {
    $options = array();
    // Just a text area
    $options['drupal_domain'] = array(
      'field_name' => 'drupal_domain',
      'name' => 'Drupal Domain',
      'type' => 'string',
      'description' => 'NOTE: IF YOU NEED PREFIXING/ETC., USE THE DrupalCommands FEATURE SET SO THAT ONE CAN USE THE db_query COMMAND. 
      Enter the domain that these SQL commands should be run on. Leave this blank to have
      AutoPilot use the Target for your code\'s domain here. This domain will match/follow the rules
      of Drupal\'s sites folders. E.g. if you want this macro run on the sites folder maching all
      subdomains under holiday.domain.com, you should enter holiday.domain.com to match xmas.holiday.domain.com,
      or you can enter the specific subdomain that you wish to match.',
    );
    $options['sql_code'] = array(
      'field_name' => 'sql_code',
      'name' => 'SQL Code',
      'type' => 'string',
      'description' => 'Place the sql script to run here.',
      'current_value' => $this->getSQLCode(),
      'options' => array('textarea' => true),
    );
    
    return $options;
  }
  
  public function save() {
    try {
      parent::save(); // Parent will throw an error
      $try = db_query("UPDATE {ap_featureset} SET fs_extra = '%s'", serialize($this->getExtra()));
      return $try;
    }
    catch(Exception $e) {
      throw $e;
    }
    return;
  }
  
  /**
   * Override the execute method
   * We want to upload the macro module
   * and run the macro code on the target.
   * This should be done on the Code target, we only want to
   * run this on the TargetWeb instance.
   *
   * TODO:
   * Perhaps this should have the ability to be locked down to a certain
   * target to be run on.
   * 
   */
  public function execute(BuildRun $buildRun = null, Target $target = null, $return_cmds = true) {
    
    return;
    if( $target instanceof TargetMySQL ) {
      $cmd = "mysql -u $username -p$password $db_name < $remote_file_path";
    }
    
  }
  
  
  protected function getFeatureSetDisplayInfo() {
    return array('name' => 'Drupal Commands/Script Feature Set');
  }
  public function getSQLCode() {
    return $this->sql_code;
  }
  public function setSQLCode($code) {
    $this->sql_code = $code;
  }
  public function getDrupalDomain() {
    return $this->drupal_domain;
  }
  public function setDrupalDomain($domain) {
    $this->drupal_domain = $domain;
  }
  
  public function setOptions($options) {
    // Set the build options defule
    $this->sql_code = $options['sql_code'];
    $this->drupal_domain = $options['drupal_domain'];
  }
  
}

?>