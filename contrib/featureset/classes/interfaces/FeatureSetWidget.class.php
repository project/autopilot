<?php

/**
 * This is the interface for a FeatureSet.
 */

interface FeatureSetWidget   {
  // This is the name/name_space for this widget
  public function getName();
  // Registers this 
  // This is a note about why this feature set is run
  public function getDescription();
  public function setDescription($desc);
  // Weight controls the order of execution
  public function setWeight($weight);
  public function getWeight();
  
  // Shows the optional items
  public function featureSetOptions();
  
  // This executes a Feature Set
  public function execute(BuildRun $buildRun = null, Target $target = null, $return_cmds = true);
  
  // Logger
  public function setLogger($logger_name);
  
}

?>