AutoPilot 2.0
=============

AutoPilot is a build and deployment management system for Drupal. Using AutoPilot, you can easily migrate code and configuration changes from development environments to QA and production environments. This document will help you get started with AutoPilot.

CONTENTS OF THIS FILE
---------------------

 * Requirements
 * Optional Tasks
 * Pear Version Control Installation
 * Installation
 * Basic Setup

REQUIREMENTS
------------

AutoPilot requires Drupal 5.x and PHP 5 (5.2.0 or greater) (http://www.php.net/).

The Pear VersionControl_SVN is also required to use the SVN repository.

It also requires the PHP SSH2 library (http://pecl.php.net/package/ssh2). This 
will probably have to be compiled from source as it is not a standard library.

The PHP mcrypt library is also required (http://php.net/mcrypt).

For more detailed information on the php ssh2 library, see the Workhabit Blog post (TBD) 
on the Workhabit site.

PEAR VERSIONCONTROL INSTALLATION
--------------------------------

1. INSTALL PEAR

	Make sure that PEAR is installed on your system (http://pear.net).

2. INSTALL VERSIONCONTROL

	Install the VersionControl_SVN pear module. You may have to use the "-alpha" mod.
	
	pear install VersionControl_SVN-alpha


INSTALLATION
------------

1. DOWNLOAD DRUPAL

	Download Drupal (http://www.drupal.org) and install Drupal as you normally would.

2. DOWNLOAD AutoPilot AND DEPENDENCIES
	
	You will need to download AutoPilot from the Workhabit repositories.
	
	You will also need to download the following dependent modules from the Workhabit
	repository:
		- Ponto Version Control (ponto_vc)
		- Ponto KeyChains (ponto_keychains)
		- Subversion implementation for Ponto Version Control (svn)

3. ENABLE THE MODULES
	Enable the following modules:
		- AutoPilot
		- AutoPilot Feature Sets
		- Ponto Keychains
		- Ponto VC
		- SVN for Ponto VC

BASIC SETUP
-----------

After AutoPilot and it's modules are installed, there are some basic setup procedures that one should implement.

1. INITIALIZE THE KEYCHAIN KEY

	Navigate to the settings page for the Ponto Keychain (admin/settings/keychains).
	Click "save" to set that as the crypt key to use. 

2. CREATE A KEYCHAIN(S)

	A Keychain is the way one authenticates to various resources. With that said, it 
	follows that it is a very important building block of the system. To start off 
	it is recommended that one creates keychains to authenticate to the code repository 
	and to the environment boxes where one will be building their sites to, as you will 
	need these later.
	
	When you create them be SURE to create SYSTEM keychain types.

3. SETUP A BASIC REPOSITORY

	Navigate to the ponto_vc settings to setup a code repository to use (admin/ponto_vc).
	Use the "Add Repository" section to setup a repository. 
	
	You may want to click "update logs" a few times to go ahead and get the logs read in 
	if your cron has not run yet. It should be noted that the logs are refreshed only on
	cron runs unless they are manually triggered using this page.
	
4. CREATE AN AUTOPILOT

	Now we can create an AutoPilot (node/add/autopilot). Just give it a name and description.
	Something like:
	
		Title: My Quotes Project
		Description: This is my personal project. This AutoPilot houses all the 
		buildplans and information for my "Quotes" project.

5. CREATE A WEB TARGET

	One we have our AutoPilot, we can start to create some items that the AutoPilot (AP) 
	will need to do its job. One of these is a Web Target. This is basically the place 
	that your web pages are served from and/or your web server (most likely just a linux box).
	
		- Click the "Setup" tab while inside your AutoPilot.
		- Click "Targets"
		- Click "Add target"
		- Select "Web/HTTP Target Server" from the drop down
		- Enter in the necessary information.

6. CREATE A MYSQL TARGET

	Now we need to create a MySQL target. This is where the database resides.
		- Click the "Setup" tab while inside your AutoPilot.
		- Click "Targets"
		- Click "Add target"
		- Select "MySQL Target Server" from the drop down (Postgres coming soon)
		- Enter in the necessary information.

7. CREATE A BUILD

	A "build" is basically our code bundle. A build is composed of:
		1) Repository root path
		2) Path in the repository to the code.
		
	So if your repository is: 
		http://svn.example.com/myproject
		
	Then your "build" for your code maybe:
		/html ==> (which is: http://svn.example.com/myproject/html)
		
	And your SQL "build" maybe:
		/sql ==> (which is: http://svn.example.com/myproject/sql)
	
		1) Click "Setup"
		2) Click "Builds"
		3) Example the "Add New Build"
		4) Select your repository from the drop down
		5) Select the path to your code in the "Path" section
		6) Give it a title
		7) Select the type of build (code or SQL)
		8) Click "Add Build" ( This adds it as a general "build" you can select and add 
		    to different project/AP's.)
		9) Expand the "Add Existing Build" section
		10) Select the build your just created.
		11) Click "Add Build to AutoPilot"
		
8. CREATE A BUILDPLAN

	Now we can create a Build Plan. This is what actually creates our site. It is the 
	collection of targets and builds that makes our build plan.
	
	1) Click "Create new Buildplan" while inside your AP
	2) Give the Buildplan a title (HEAD - Quotes Project)
	3) Give a description.
	4) Select a "Label"
	
	Build Matrix
	
		You will be presented with a "Build Matrix". 
		The rows on the table represent an "Endpoint", like a webserver 
		or database server.
		
		The columns of the table represent the "builds" or "codes" and where that 
		should go.
		So if you are reading the table from left to right, the first row would 
		read something like:
		
		"For my webserver (col. 1), I want my trunk code (col 2) to go to my 
		development server (col 3).".
		
		You will notice in the drop downs that you can select any of the targets 
		and/or builds that you have created.
		
	5) Click "Create Build Plan"

9. RUN A BUILDPLAN

	Select the build plan you have just created by clicking "view" on the "Buildplan" 
	section of your AP.
	
	From there, select what repository version you wish to use, and just click 
	"Run Build".