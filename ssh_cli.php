<?php
/**
 * Usage:
 * -u username
 * -p password
 * --port port
 * --local local path
 * --remote remote path
 * -c Command which can be one of:
 *  * scp
 *  * scp_pull
 * CLI Version to get around the error in the APACHE currently
 * that will not allow this extension to be loaded.
 */

# Setup and Input parsing
$args = $argv;
unset($args[0]);
$input = parseArguments($args);
if(isset($input['command'])) {
  $input['command'] = base64_decode($input['command']);
}


# Create Object
$shell = new SSH2($input['host'], $input['port']);
$shell->authPassword($input['username'], $input['password']);

# Look up the command 
$lookup_cmd = $input['action'];
if($lookup_cmd == 'scp') {
  $shell->pushToRemote($input['local'], $input['remote']);
}
else if($lookup_cmd == 'scp_pull') {
  $output = $shell->pullFromRemote($input['remote'], $input['local']);
  print $output;
}
else {
  $output = $shell->cmdExec($input['command']);
  print $output;
  unset($shell);
}
exit();

// ssh protocols
// note: once openShell method is used, cmdExec does not work

class SSH2 {

  private $host = 'host';
  private $user = 'user';
  private $port = '22';
  private $password = 'password';
  private $con = null;
  private $shell_type = 'bash';
  private $shell = null;
  private $logs = array();
  private $connection_open = false;

  function __construct($host='', $port=''  ) {
      if (!extension_loaded('ssh2')) {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            dl('php_ssh2.dll');
        } else {
            dl('ssh2.so');
        }
      } 
     if( $host!='' ) $this->host  = $host;
     if( $port!='' ) $this->port  = $port;

     $this->con  = ssh2_connect($this->host, $this->port);
     if( !$this->con ) {
       $this->connection_open = false;
       $this->throwFatalError("Connection failed  to $this->host on $this->port");
     }

  }
  function logger($msg) {
    $this->logs[] = $msg;
  }
  function getLogs() {
    return $this->logs;
  }
  
  function pushToRemote($local_path, $remote_path) {
    if(AUTOPILOT_DEBUG_MODE === true) {
      $logger =& LoggerManager::getLogger('pushToRemote');
      $logger->debug("Pushing $local_path to $remote_path");
    }
    
    umask(0);

    $try = ssh2_scp_send($this->con, $local_path, $remote_path, 0777);
    if(!$try) {
      $this->throwFatalError("Channel SCP: Unable to SCP Copy: [local] $local_path, [remote] $remote_path");
    }
    else {
      #$logger->info("Copy sucessful...");
    }
    return;
  }
  
  function pullFromRemote($remote_path, $local_path) {
    if(AUTOPILOT_DEBUG_MODE === true) {
      $logger =& LoggerManager::getLogger('pullFromRemote');
      $logger->debug("Pushing $local_path to $remote_path");
    }
    umask(0);
    $try = ssh2_scp_recv($this->con, $remote_path, $local_path);
    if(!$try) {
      $this->throwFatalError("Channel SCP: Unable to SCP Pull to $local_path from $remote_path");
    }
    else if(AUTOPILOT_DEBUG_MODE === true) {
      $logger->info("Pull sucessful");
    }
    return;
  }
  
  function isConnectionOpen() {
    return $this->connection_open;
  }

  function authPassword( $user = '', $password = '' ) {

     if( $user!='' ) $this->user  = $user;
     if( $password!='' ) $this->password  = $password;

     if( !ssh2_auth_password( $this->con, $this->user, $this->password ) ) {
       $this->logger("Authorization failed for $this->user");
       $this->throwFatalError("Authorization failed for $this->user");
     }

  }

  function openShell( $shell_type = '' ) {
    if ( $shell_type != '' ) {
      $this->shell_type = $shell_type;
    }
    
    $this->shell = ssh2_shell( $this->con,  $this->shell_type );
    
    if( !$this->shell ) {
      $this->throwFatalError("Shell connection failed");
    }
    return;

  }

  function writeShell( $command = '' ) {
    fwrite($this->shell, $command."\n")
    or
    $this->throwFatalError("Error while writing to shell");
  }

  function cmdExec($cmd) {
    $cmd = "echo '[start]';$cmd;echo '[end]'";
    $stream = ssh2_exec( $this->con, $cmd, false );
    stream_set_blocking( $stream, true );
    $contents = stream_get_contents($stream);
    //fclose($stream);
    return $contents;

  }

  public function getLog() {
     return implode("\n", $this->getLogs());
  }
  
  /**
   * The CLIFatalError echos the
   * error message and then exits
   * with an exit code other than 0.
   * TODO: Perhaps match up error code
   * types?
   */
  public function CLIFatalError($msg = null) {
    if($msg !== null) {
      $msg = implode("\n", $this->getLogs());
    }
    ob_end_clean();
    print $msg;
    ob_flush();
    exit(1);
  }
  
  public function throwFatalError($msg) {
    $this->CLIFatalError($msg);
  }

}

function parseArguments($argv) {
    $parsed = array();
    foreach ($argv as $arg) {
        if (preg_match('#^-{1,2}([a-zA-Z0-9]*)=?(.*)$#', $arg, $matches)) {
            $key = $matches[1];
            switch ($matches[2]) {
                case '':
                case 'true':
                $arg = true;
                break;
                case 'false':
                $arg = false;
                break;
                default:
                $arg = $matches[2];
            }
           
            /* make unix like -afd == -a -f -d */           
            if(preg_match("/^-([a-zA-Z0-9]+)/", $matches[0], $match)) {
                $string = $match[1];
                for($i=0; strlen($string) > $i; $i++) {
                    $parsed[$string[$i]] = true;
                }
            } else {
                $parsed[$key] = $arg;   
            }           
        } else {
            $parsed['input'][] = $arg;
        }       
    }
    return $parsed;   
}

