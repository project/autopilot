<?php

/**
 *
 *
 *
 */

/**
 * Main overview of the targets.
 */
function targets_page($tid = null) {
  global $user;
  
  // Dispatch
  if(arg(4) == 'edit' || arg(3) == 'add') {
    $tid = arg(3) == 'add' ? null : arg(3);
    $page .= drupal_get_form('target_edit_form', $tid);
    return $page;
  }
  elseif(arg(4) == 'delete') {
    return drupal_get_form('target_delete_form', arg(3));
  }
  elseif(arg(4) == 'remove') {
    $tid = arg(3);
    $target = Target::targetFactory($tid);
    $try = $target->removeFromAutoPilot();
    if($try) {
      drupal_set_message(t('%name was removed from your AutoPilot.',
                           array('%name' => $target->getName()))
                        );
    }
    else {
      drupal_set_message(t('There was a problem removing %name from your AutoPilot.',
                           array('%name' => $target->getName()),
                           'error')
                        );    
    }
    drupal_goto('autopilot/setup/targets');
  }
  
  $page = '';
  $page .= "<h2>" . t("Targets") . "</h2>";
  $page .= theme('ap_fieldset', t('Availiable Targets'), drupal_get_form('target_add_existing_form'));
  if(user_access('autopilot create target')) {
    $page .= theme('image',
      drupal_get_path('module', 'autopilot') . '/media/images/building_add.png' )
      . ' - ' . l(t('Add target'), 'autopilot/setup/targets/add');
  }
  // Show the current targets
  $header = array(t('Name'), t('Address'), '-');
  $rows = array();
  $row = array();
  $rs = db_query("SELECT * FROM {ap_target} apt INNER JOIN {ap_targets} apts ON apt.tid = apts.tid
                 WHERE apts.apid = %d", AutoPilot::getCurrentAutoPilotID());
  while($target = db_fetch_object($rs)) {
    $row = array();
    $row[] = $target->target_name;
    $row[] = $target->target_address;
    $links = array();
    $links[] = array('title' => t('edit'), 'href' => "autopilot/setup/targets/$target->tid/edit");
    $links[] = array('title' => t('remove'), 'href' => "autopilot/setup/targets/$target->tid/remove");
    $links[] = array('title' => t('delete'), 'href' => "autopilot/setup/targets/$target->tid/delete");
    $row[] = theme('links', $links);
    $rows[] = $row;
  }
  if(db_num_rows($rs)) {
    $page .= theme('table', $header, $rows, t('Your Availiable Targets'));
  }
  else {
    $page .= '<h2>' . t('No targets found.') . '</h2>';
  }
  
  return $page;
}

/**
 * Add a target to an AutoPilot
 */
function target_add_existing_form() {
  $form = array();
  $targets = array();
  $rs = db_query("SELECT apt.tid, apt.target_name
                 FROM {ap_target} apt LEFT JOIN {ap_targets} apts ON apt.tid = apts.tid
                 WHERE (apts.apid IS NULL || apts.apid != %d)",
                 AutoPilot::setCurrentAutoPilot());
  while($row = db_fetch_object($rs)) {
    $targets[$row->tid] = $row->target_name;
  }
  if( count($targets) == 0 ) {
    $form['nada'] = array(
      '#value' => t('No availiable targets found.')
    );
    return $form;
  }
  $form['tid'] = array(
    '#type' => 'select',
    '#title' => t('Target to Add'),
    '#options' => $targets,
    '#description' => t('Select an availiable target to add to the AutoPilot.'),
  );
  $form['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add Target to AutoPilot'),
  );
  return $form;
}

function target_add_existing_form_submit($form_id, $form_values) {
  $target = Target::targetFactory($form_values['tid']);
  $try = $target->addToAutoPilot();
  if($try) {
    drupal_set_message(t('%name was added to your AutoPilot.',
                         array('%name' => $target->getName()))
                      );
  }
  else {
    drupal_set_message(t('There was a problem adding %name to your AutoPilot.',
                         array('%name' => $target->getName()),
                         'error')
                      );    
  }
  return;
}

/**
 * Delete a target
 */
function target_delete_form($tid) {
  $target = Target::targetFactory($tid);
  $form = array();
  $form['tid'] = array('#type' => 'hidden',
                       '#value' => $tid);
  $confirm_msg = t('Are you sure you with to remove %target_name ?',
                   array('%target_name' => $target->getName()));
  return confirm_form($form, $confirm_msg, 'autopilot/setup/targets');
}
function target_delete_form_submit($form_id, $form_values) {
  $target = Target::targetFactory($form_values['tid']);
  db_query("DELETE FROM {ap_target} WHERE tid = %d", $target->getID());
  drupal_set_message(t('The target %target_name has been removed.'),
                     array('%target_name' => $target->getName()));
  return 'autopilot/setup/targets';
}

/**
 * Create a new target
 */
function target_edit_form($tid = null) {
  global $user;
  
  $form = array();
  $form['#wizard'] = true;
  $form['#redirect'] = false;
  
  if($tid != null) {
    $target = Target::targetFactory($tid);
  }
  else {
    $target = new Target();
  }

  $_current_type = $_POST['target_type'];
  $form['_target_current_type'] = array('#type' => 'hidden',
                                        '#value' => $_current_type);
  // Types
  $target_types = Target::targetFactory();
  $form['ttype'] = array(
    '#type' => 'fieldset',
    '#title' => t('Type'),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $form['ttype']['target_type'] = array(
    '#type' => 'select',
    '#title' => t('Target type'),
    '#options' => $target_types,
    '#default_value' => $_POST['target_type'] ? $_POST['target_type'] : $target->getType());
  $form['ttype']['update_type'] = array(
    '#type' => 'submit',
    '#value' => t('Update Type'),
  );
  
  // Stop here
  if($_POST['target_type'] == '' && !is_numeric(arg(3))) {
    return $form;
  }
  if($tid != null) {
    Target::targetFactory($tid);
  }
  else {
    $classname = $_POST['target_type'];
    $target = new $classname();
  }
  
  // Generic Target options
  $form['target_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Target Name'),
    '#description' => t('A title to identify this target.'),
    '#default_value' => $target->getName());
  $form['target_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Target Description'),
    '#description' => t('Description'),
    '#default_value' => $target->getDescription());
  $form['target_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Target Address'),
    '#description' => t('This is the address to the target. This can be either
                        a network share path, and IP address, a web address,
                        etc.
                        DO NOT INLUCDE THE PROTOCOL IN THE ADDRESS!
                        E.G. NOT http://localhost, but just localhost.'),
    '#default_value' => $target->getAddress());
  $form['target_env'] = array(
    '#type' => 'textarea',
    '#title' => t('Target Environment'),
    '#description' => t('Seperate each environment setting with a new line.'),
    '#default_value' => $target->getEnv(),
  );
  
  // Options specific to this type
  $form['target_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Target Options'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
  );
  $options = $target->targetOptions();
  $form['target_extra']['#tree'] = true;
  foreach($options as $k => $option) {
    $form['target_options']['target_extra'][$option['field_name']] = array(
      '#type' => $option['type'] == 'string' ? 'textfield' : 'select',
      '#title' => $option['name'],
      '#description' => $option['description'],
      '#default_value' => $option['current_value'],
    );
    if($option['type'] == 'select') {
      $form['target_options']['target_extra'][$option['field_name']]['#options'] = $option['options'];
    }
  }
  
  // If the type is not selected they need to
  $form['target_paths'] = array(
    '#type' => 'fieldset',
    '#title' => t('Target Paths'),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $form['target_paths']['target_build_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Build Path'),
    '#default_value' => $target->getBuildPath(),
    '#description' => t('This is the area where all the builds will be placed
                        on the target machine.'),
  );
  $form['target_paths']['target_backup_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Backup Path'),
    '#default_value' => $target->getBackupPath(),
    '#description' => t('This is path to the area where backups of the current
                        build will be placed when running a build plan.'),
  );
  $form['target_paths']['target_tmp_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Remote TMP Path'),
    '#default_value' => $target->getRemoteTempPath(),
    '#description' => t('This is the path on the remote target that can be
                        used as a "temporary" area.'),
  );
  $form['target_paths']['target_wpath'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Working Path'),
    '#description' => t('This is the working path that AutoPilot will change
                        to on the target when working. You can leave this
                        blank if you wish to use the default directory. This is
                        just the starting path, if will move around to different
                        directories as needed.'),
    '#default_value' => $target->getDefalutWorkingPath());
  $form['target_ownership'] = array(
    '#type' => 'select',
    '#title' => t('Ownership'),
    '#options' => _autopilot_get_owernships('select'),
    '#default_value' => $target->getOwnership());
  
  $form['target_auth_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Target Authentication Settings'),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  
  // Auth Settings
  $current_auth_settings = $target->getAuthSettings();
  $keychains = array('' => '[' . t('Use Target Settings') . ']');
  $keychains += _ponto_keychain_get_keychains();

  $form['target_auth_settings']['kcid'] = array(
    '#type' => 'select',
    '#title' => t('Keychain'),
    '#options' => $keychains,
    '#default_value' => $target->getKeychainID(),
    '#description' => t('Either select %this_settings or select a Keychain
                        from the list here to user the Authentication
                        settings in that Keychain to authenticate to the
                        target.',
                        array('%this_settings' => t('Use Target Settings')))
  );
  // Get the keychain that was set.
  $form['target_auth_settings']['target_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => $current_auth_settings->username);
  $form['target_auth_settings']['target_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => $current_auth_settings->password);
  
  if($tid) {
    $form['tid'] = array('#type' => 'hidden', '#value' => $tid);
    $form['update_target'] = array(
      '#type' => 'submit',
      '#value' => t('Update Target'));
  }
  else {
  $form['add_target'] = array(
    '#type' => 'submit',
    '#value' => t('Add Target'));
  }
  
  return $form;
}

/**
 * Validae the form
 */
function target_edit_form_validate($form_id, $form_values) {
  
  if(arg(3) != 'add' &&
     ($form_values['_target_current_type'] != $form_values['target_type'] ||
     ( !isset($form_values['tid']) && $form_values['op'] != t('Add Target') ))) {
    form_set_error('target_current_type', t('The type of target has been changed.
      Please update your settings and then attempt to save again.'));
  }
  elseif(arg(3) == 'add' && $form_values['op'] != t('Add Target')) {
    form_set_error('target_current_type', t('Please enter in the information for the new target.'));
  }
  
  return;
}

/**
 * Creates/Updates a target
 */
function target_edit_form_submit($form_id, $form_values) {
  global $user;
  $target = Target::targetFactory($form_values['tid'], $form_values['target_type']);

  if(!isset($form_values['tid'])) {
    $target->createNew();
    $new = true;
  }
  $target->setName($form_values['target_name']);
  $target->setWorkingPath($form_values['target_wpath']);
  $target->setBuildPath($form_values['target_build_path']);
  $target->setBackupPath($form_values['target_backup_path']);
  $target->setRemoteTempPath($form_values['target_tmp_path']);
  $target->setAddress($form_values['target_address']);
  $target->setOwnership($form_values['target_ownership']);
  $target->setEnv($form_values['target_env']);
  // TODO: Instead of storying here, create a new system keychain
  $target->setUsername($form_values['target_username']);
  $target->setPassword($form_values['target_password']);
  if(is_numeric($form_values['kcid'])) {
    $keychain = new PontoKeyChain($form_values['kcid']);
    $target->setKeyChain($keychain);
  }
  
  // Options
  $target->setOptions($form_values['target_options']['target_extra']);
  
  $save = $target->save();
  if($save) {
    if($new) {
      drupal_set_message(t('Your new target %target_name has been created.',
                           array('%target_name' => $form_values['target_name'])));
    }
    else {
      drupal_set_message(t('Your new target has been updated.'));
    }
  }
  else {
    drupal_set_message(t('There was an error updating your target.'), 'error');
  }
  // TODO: Return not working. Stays on the page, should we use
  // drupal_goto instead?
  return 'autopilot/setup/targets';
}
