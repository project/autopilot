<?php

class APSimpleLogger implements APLoggableInterface {
  var $log_types = array(
    'info',
    'warn',
    'fatal',
  );
  private $logger = null;
  private $logger_name = '';
  
  function __construct($logger_name = null) {
    if($logger_name == null) {
      $logger_name = 'Default_Logger';
    }
    $this->setLogger($logger_name);
  }
  // Set Logger
  function setLogger($logger_name) {
    if($logger_name instanceof APSimpleLogger) {
      $logger_name = $logger_name->getLoggerName();
    }
    $this->logger = LoggerManager::getLogger($logger_name);
    $this->logger_name = $logger_name;
  }
  public function getLogger() {
    return $this->logger;
  }
  public function getLoggerName() {
    return $this->logger_name;
  }
  public function log($type = 'info', $msg = null) {
    if(!@in_array($type, $log_types)) {
      $type = 'info';
    }
    $this->logger->$type(addslashes($msg));
  }
}
