<?php

class Command {
  private $cmd = '';
  private $key_chain = null;
  private $username = null;
  private $password = null;
  private $lcwd = '.';
  private $rcwd = '.';
  private $channel = null;
  private $output = null;
  private $use_escapeshellcmd = true;
  private $passthru = false;
  private $ret_var = null;
  private $pipe = '2>&1';
  
  function __construct() {
    
    if(_autopilot_detect_os() == AUTOPILOT_OSTYPE_WINDOWS) {
      $this->use_escapeshellcmd = false;
    }
    
    return;
  }
  
  /**
   * Set the Channel we are working with.
   * Should we set the target instead?
   */
  function setChannel(Channel $ch) {
    $this->channel = $ch;
  }
  
  /**
   * Run the command and gather the output.
   * 
   */
  function runCommand($cmd = null, $pipe = true) {
    if($pipe) {
      $pipe = $this->pipe;
    }
    else {
      $pipe = '';
    }
    
    $this->output = '';
    if($cmd === null) {
      $cmd = $this->cmd;
    }

    if ($this->use_escapeshellcmd) {
        $cmd = escapeshellcmd($cmd);
    }
    
    if (!$this->passthru) {
        exec("{$this->prepend_cmd}$cmd $pipe", $out, $ret_var);
    } else {
        passthru("{$this->prepend_cmd}$cmd $pipe", $ret_var);
    }
    
    
    $this->output = $out;
    $this->ret_var = $ret_var;
    
    return $ret_var;
  }
  
  function getOutput() {
    return $this->output;
  }
  
  function getReturnCode() {
    return $this->ret_var;
  }
  
  /**
   * @abstract Allows someone to get and set if this command should
   * run as a passthru.
   * 
   */
  function passthru($pass = null) {
    if($pass != null) {
      $this->passthru = $pass;
    }
    return $this->passthru;
  }
  
  function useRawCommand() {
    $this->use_escapeshellcmd = false;
  }
  
  function escapeCommand($escape_command) {
    $this->use_escapeshellcmd = $escape_command;
  }
  
}

