<?php

interface SSH2Interface {
  function setHost($host);
  function setPort($port);
  function sshRunRemoteCommand($remote_cmd, $escape_shell = true);
  function scpCopyToRemote($local_file, $remote_file);
  function scpPullFromRemote($remote_file_path, $local_file_path);
  function isConnectionOpen();
  function authPassword($user = '', $password = '');
  function openShell($shell_type = '');
  function writeShell($command = '');
  function cmdExec($cmd);
  function setUser($user);
  function setPassword($password);
  function setKeyChain($kc);
}
