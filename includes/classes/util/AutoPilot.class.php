<?php

class AutoPilot {
  private $apid = null;
  private $create_new = false;
  private $loaded = false;
  private $ap_name = '';
  
  // Types
  
  public function __construct($apid) {
    $this->apid = $apid;
    $node = node_load($apid);
    $this->ap_name = $node->title;
  }
  
  public function getID() {
    return $this->apid;
  }
  
  public function getName() {
    return $this->ap_name;
  }
  
  /**
   * This is for later when we are
   * going to create autopilots and
   * hold more information about them in our other tables.
   */
  public function createNew() {
    if($this->loaded) {
      throw new Error('Can not create a new autopiot from another instance');
    }
    $this->create_new = true;
  }
  
  /**
   * @abstract Sets the current autopilot.
   * If given an APID, it will set the current autopilot to that ID.
   *
   * @param $mixed If given an APID, it will set it. Given null, returns the current apid
   * @param $mixed current AutoPilot instance
   */
  public static function setCurrentAutoPilot($set_apid = null) {
    static $autopilot;
    static $apid;
    if($set_apid != null) {
      $autopilot = new AutoPilot($set_apid);
      $apid = $set_apid;
    }
    
    return $autopilot;
  }

  // TODO: Throw an error here if no AutoPIlot is set
  public static function getCurrentAutoPilotID() {
    $autopilot = AutoPilot::setCurrentAutoPilot();
    if($autopilot instanceof AutoPilot) {
      return $autopilot->getID();
    }
    else {
      return null;
    }
  }
  
  public static function getAPTypes() {
    return array(
      'CODE' => t('Code'),
      'SQL' => t('SQL/DB'),
    );
  }
  
  /**
   * Returns information about the lables in the system.
   */
  public static function getLables() {
    
  }
  
}

