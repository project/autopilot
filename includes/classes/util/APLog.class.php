<?php

class APLog {
  
  private $runningLog = array();
  
  
  public function log($text) {
    $this->runningLog[] = $text;
  }
  
  public function clearLog() {
    $this->runningLog = array();
  }
  public function getLog() {
    return $this->runningLog;
  }
  
  /**
   * This returns an instace of the logger.
   * We want to use a static function so that
   * if we are currently in a buildrun, we return
   * an APLog that is setup correctly.
   */
  public static function loggerFactory($name = '') {
    if(APLog::buildRun) {
      $logger = LoggerManager::getLogger(APLog::getBuildRunName);
      return $logger;
    }
    else {
      $logger = LoggerManager::getLogger($name);
    }
  }
}
