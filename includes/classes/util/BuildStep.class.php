<?php

class BuildStep {
  private $buildstep_info = null;
  private $sid = null;
  private $create_new = false;
  private $bpid = null;
  private $step_number = 0;
  private $step_type_ref = null;
  
  public function __construct($sid = null) {
    if(is_numeric($sid)) {
      $this->load($sid);
    }
    else {
      $this->buildstep_info = new stdClass();
    }
    return;
  }
  
  public function save() {
    global $user;
    
    if($this->create_new && $this->sid == null) {
      $sid = db_next_id("{ap_build_plan_steps}_sid");
      $try = db_query("INSERT INTO {ap_build_plan_steps}(sid, bpid, uid)
                      VALUES(%d, %d, %d)",
                      $sid, $this->bpid, $user->uid);
      if(!$try) {
        db_query("DELETE FROM {ap_build_plan_steps} WHERE sid = %d", $sid);
        return false;
      }
      $this->sid = $sid;
    }
    // Update
    // First, check the step number.
    // TODO: Fix ordering 
    if(db_result(db_query("SELECT COUNT(*) FROM {ap_build_plan_steps} WHERE bpid = %d", $this->bpid))) {
      db_query("UPDATE {ap_build_plan_steps} SET step_number = step_number + 1 WHERE step_number >= %d", $this->step_number);
    }
    $save = db_query("UPDATE {ap_build_plan_steps}
                     SET bpid = %d, uid = %d, step_name = '%s',
                     step_type = '%s', step_ref_id = %d,
                     step_number = %d 
                     WHERE sid = %d",
                     $this->bpid, $user->uid, $this->getName(),
                     $this->getType(), $this->getRefID(),
                     $this->step_number,
                     $this->sid);
    if(!$save && $this->create_new) {
      db_query("DELETE FROM {ap_build_plan_steps} WHERE sid = %d", $sid);
    }
    return $save;
    
  }
  
  /**
   * This executes the Step.
   * This is different depnding on the step.
   *
   * @return boolean Good execution of error on execution
   */
  public function execute() {
    // What we are really doing
    // is calling the execute
    // method of the item we are on.
    // It will know how to execute because we are going to set
    // its BP reference.
    // TODO: We need to also set a "runID" reference.
    $step_ref = new stdClass();
    $step_ref = $this->getRef();
    if(!is_object($step_ref)) {
      throw new Exception(t('Error: BuildStep reference not found.'), 1);
    }
    $bp = new BuildPlan($this->bpid);
    
    $step_ref->setBuildPlanRef($bp);
    
    //$step_ref->setExecutionOptions($option, $val);
    # Execute
    $ret_var = $step_ref->execute();
    return $ret_var;
  }
  
  
  public function createNew() {
    $this->create_new = true;
    $this->buildstep_info = new stdClass();
    $this->sid = null;
    $this->bpid = null;
    return;
  }
  
  public function load($sid) {
    $buildstep_info = db_fetch_object(db_query("SELECT *
      FROM {ap_build_plan_steps}
      WHERE sid = %d", $sid));
    $this->buildstep_info = $buildstep_info;
    $this->sid = $this->buildstep_info->sid;
    $this->bpid = $this->buildstep_info->bpid;
    
    // Load the step
    // TODO: Make this totally dynamic and extendable to
    // different types.
    if( $buildstep_info->step_type == 'build') {
      $this->step_type_ref = Build::buildFactory($buildstep_info->step_ref_id);
    }
    else if( $buildstep_info->step_type == 'target') {
      $this->step_type_ref = Target::targetFactory($buildstep_info->step_ref_id);
    }
    
    return;
  }
  
  public function getName() {
    return $this->buildstep_info->step_name;
  }
  public function setName($name) {
    $this->buildstep_info->step_name = $name;
  }
  
  public function getStepNumber() {
    return $this->buildstep_info->step_number;
  }
  public function setStepNumber($step) {
    $this->buildstep_info->step_number = $step;
    $this->step_number = $step;
    return;
  }
  
  public function getType() {
    return $this->buildstep_info->step_type;
  }
  public function setType($type) {
    $this->buildstep_info->step_type = $type;
  }
  
  public function getRefID() {
    return $this->buildstep_info->step_ref_id;
  }
  /**
   * To set a new reference id, you must set
   * the type that this reference referes to.
   */
  public function setRefID($id, $type) {
    $this->buildstep_info->step_ref_id = $id;
    $this->setType($type);
    return;
  }
  public function getRef() {
    return $this->step_type_ref;
  }
  
  public function getBuildPlanRefID() {
    return $this->bpid;
  }
  public function setBuildPlanRefID($bpid) {
    $this->bpid = $bpid;
  }
  public function setBuildPlanRef(BuildPlan $buildplan) {
    $this->buildplan_info = $buildplan;
  }
  
}
