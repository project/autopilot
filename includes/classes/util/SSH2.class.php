<?php
require_once('SSH2Interface.class.php');
// ssh protocols
// note: once openShell method is used, cmdExec does not work
define('SSH2_MODE_NORMAL', 1);
define('SSH2_MODE_CLI', 2);
class SSH2 implements APLoggableInterface, SSH2Interface {

  private $host = 'host';
  private $user = 'user';
  private $port = '22';
  private $password = 'password';
  private $con = null;
  private $shell_type = 'xterm';
  private $shell = null;
  private $log = '';
  private $mode = SSH2_MODE_NORMAL;
  private $APLogger;
  private $UserKeyChain = null;
  private $useKeyChain = false;
  public static $php_exec = 'php';
  
  private static function sshCheckMode() {
    $os = _autopilot_detect_os();
    $mode_check = '';
    $is_cgi_mode = stristr($_SERVER['GATEWAY_INTERFACE'], 'CGI');
    if( $os['type'] == AUTOPILOT_OSTYPE_WINDOWS && $is_cgi_mode ) {
      $mode_check = SSH2_MODE_CLI;
    }
    else {
      $mode_check = SSH2_MODE_NORMAL;
      // We need this here for Windows running in FCGI
      // thus why we're only concerned about .dll and not .so.
      if (!extension_loaded('ssh2')) {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            dl('php_ssh2.dll');
        } else {
            dl('ssh2.so');
        }
      }
    }
  }
  
  public function setHost($host) {
    $this->host = $host;
  }
  public function setPort($port) {
    $this->port = $port;
  }
  
  public function sshRunRemoteCommand($remote_cmd, $escape_shell = true) {
    // FOR NOW, RUN IN CLI ONLY.
    // SSH2 hangs not run in CLI mode.
    
    /**
     * Set to always true for now because the ssh2 module
     * currently doesn't fork.
     * This may change in the future, but not that this currently a
     * totalitarian logic expression ( meaning it will ALWAYS be true).
     */
    if( SSH2::sshCheckMode() == SSH2_MODE_CLI || true ) {
      $exec_path = drupal_get_path('module', 'autopilot') . '/ssh_cli.php';
      $cmd = new Command();
      $php_exec = SSH2::$php_exec;
      // Comiles
      $sshcmd = "$php_exec $exec_path --host=$host --username=$username --password=$pass --command=" . base64_encode($remote_cmd);
      $cmd->escapeCommand($escape_shell);
      $this->logger->log(t('Running command'));
      $cmd->runCommand($sshcmd, false);
      # Check the return code
      if($cmd->getReturnCode() != 0) {
        foreach($cmd->getOutput() as $k => $line) {
          $line = trim($line);
          if($line != '[start]' && $line != '[end]') {
            $this->logger->log('fatal', '[SSH2] ' . $line);
            $error_msg .= $line;
          }
        }
        throw new Exception(t('SSH2 Exception: %msg', array('%msg' => substr($error_msg, 0, 100))));
      }
      $output = $cmd->getOutput();
    }
    // Run as SSH2 object
    else {
      $ssh2 = new SSH2($host);
      $ssh2->setUser($username);
      $ssh2->setPassword($pass);
      $output = $ssh2->cmdExec($remote_cmd . ' 2>&1');
      
    }
    
    // Log the output
    if(is_array($output)) {
      foreach($output as $k => $line) {
        $line = trim($line);
        if($line != '[start]' && $line != '[end]') {
          $this->logger->log('debug', '[SSH2] ' . $line);
        }
      }
    }
    $this->logger->log(t('Command run ok!'));
    return $output;
  }
  
  function scpCopyToRemote($local_file, $remote_file) {
    $exec_path = drupal_get_path('module', 'autopilot') . '/ssh_cli.php';
    $cmd = new Command();
    $php_exec = SSH2::$php_exec;
    // Comiles
    $local_file = str_replace('\\', '/', $local_file);
    $remote_file = str_replace('\\', '/', $remote_file);
    $sshcmd = "$php_exec $exec_path --host=$host --username=$username --password=$pass --action=scp --local=\"$local_file\" --remote=\"$remote_file\"";
    $cmd->escapeCommand($escape_shell);
    $cmd->runCommand($sshcmd, false);
    $output = $cmd->getOutput();
    return $output;    
  }
  
  function scpPullFromRemote($remote_file_path, $local_file_path) {
    if($remote_file_path == "" || $local_file_path == "") {
      // LOG ERROR MSG
      return false;
    }
    $exec_path = drupal_get_path('module', 'autopilot') . '/ssh_cli.php';
    $cmd = new Command();
    $php_exec = SSH2::$php_exec;

    $data = array();
    $username = $this->kc->getUsername();
    $pass = $this->kc->getPassword();
    $sshcmd = "$php_exec $exec_path --host={$this->host} --username=$username --password=$pass --sshkeypath=$ssh_key_path --action=scp_pull --local=$local_file_path --remote=$remote_file_path";
    $cmd->useRawCommand(); // So that the quotes stay
    $cmd->runCommand($sshcmd, false);
    $output = $cmd->getOutput();
    return $output;    
  }
  
  
  function __construct($host='', $port=''  ) {
    $os = _autopilot_detect_os();
    $this->APLogger = new APSimpleLogger();
    
    $is_cgi_mode = strpos($_SERVER['GATEWAY_INTERFACE'], 'CGI') !== false;
    if( $os['type'] == AUTOPILOT_OSTYPE_WINDOWS && !$is_cgi_mode ) {
      $this->mode = SSH2_MODE_CLI;
    }
    else {
      $this->mode = SSH2_MODE_NORMAL;
      // We need this here for Windows running in FCGI
      // thus why we're only concerned about .dll and not .so.
      if(!extension_loaded('php_ssh2.dll')) {
        dl('php_ssh2.dll');
      }
    }

    if( $host!='' ) $this->host  = $host;
    if( $port!='' ) $this->port  = $port;

    $this->con  = ssh2_connect($this->host, $this->port);
    if( !$this->con ) {
      $this->log .= "Connection failed !";
    }
     
  }
  
  /**
   * Logger proxy setcion
   */
  public function setLogger($logger) {
    $this->APLogger->setLogger($logger);
  }
  public function getLogger() {
    return $this->APLogger->getLogger();
  }
  public function getLoggerName() {
    return $this->APLogger->getLoggerName();
  }
  /**
   * @author earnest dot berry at gmail dot com
   * @params Just a message will default to an info type msg.
   */
  public function log($type = 'info', $msg = null) {
    if($msg === null) {
      $msg = $type;
      $type = 'info';
    }
    if($msg == '') {
      return;
    }
    $this->APLogger->log($type, $msg);
    return;
  }
  
  
  function isConnectionOpen() {
    return $this->connection_open;
  }

  function authPassword( $user = '', $password = '' ) {

     if( $user!='' ) $this->user  = $user;
     if( $password!='' ) $this->password  = $password;

     if( !ssh2_auth_password( $this->con, $this->user, $this->password ) ) {
       $this->log .= "Authorization failed !";
     }

  }

  function openShell( $shell_type = '' ) {
    if ( $shell_type != '' ) {
      $this->shell_type = $shell_type;
    }
    
    $this->shell = ssh2_shell( $this->con,  $this->shell_type );
    
    if( !$this->shell ) {
      $this->log .= " Shell connection failed !";
    }
    return;

  }

  function writeShell( $command = '' ) {

    fwrite($this->shell, $command."\n");

  }

  /**
   * Purley a debugging command
   */
  function cmdExec($cmd) {
    $cmd = "echo '[start]';$cmd;echo '[end]'; exit;";
    error_log($cmd);
    error_log("Connection is: " . $this->con);
    $cmd = 'ls';
    $stream = ssh2_exec( $this->con, $cmd, false );
    error_log("Stream open..");
    if(!$stream) {
      error_log("NO STREAM");
    }
    error_log("Setting to blocking..");
    /**
     * Note: This does not work.
     * See bug: 36918
     */
    //stream_set_blocking( $stream, true );
    while($line = fgets($stream)) {
      error_log("Fushing..");
      flush();
      $contents[] = $line;
    }
    $contents = implode("\n", $contents);
    error_log("Getting thel ast of the contenst.");
    $contents .= stream_get_contents($stream);
    //fclose($stream);
    error_log("Contents are: $contents");
    
    die("DONE");
    return $contents;

  }

  public function getLog() {

     return $this->log;

  }
  
  public function setUser($user) {
    $this->setUsername($user);
  }
  public function setUsername($user) {
    $this->user = $user;
  }
  public function setPassword($password) {
    $this->password = $password;
  }
  public function setKeyChain($kc) {
    if($kc instanceof KeyChain) {
      $this->UserKeyChain = $kc;
      $this->useKeyChain = true;
    }
    else {
      $this->useKeyChain = false;
    }
  }

}

