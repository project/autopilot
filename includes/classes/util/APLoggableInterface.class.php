<?php

interface APLoggableInterface {
  // Set Logger
  public function setLogger($logger_name);
  public function getLogger();
  public function log($type = 'info', $msg = null);
  public function getLoggerName();
}
