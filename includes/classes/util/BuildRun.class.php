<?php
// Definitiion of run status codes
define('BUILDRUN_IN_QUEUE', 1);
define('BUILDRUN_IN_PROGRESS', 2);
define('BUILDRUN_COMPLETE', 4);
define('BUILDRUN_COMPLETE_OK',5);
define('BUILDRUN_COMPLETE_ERROR', 6);

class BuildRun {
  
  private $bp = null;
  protected $start = 0;
  protected $stop = 0;
  protected $run_status = BUILDRUN_IN_QUEUE; // Status of the current run, e.g. in progress, etc.
  protected $status; // Status of the BuildRun object. e.g. active, etc.
  private $run_id = null;
  private $build_name = '';
  private $runnable = false;
  private $initialize = false;
  private $logs = array();
  private $update_scope = 'full';
  private $lock_step = false;
  private $cache_rebuild = array();
  protected $APLogger = null;
  
  public static function getStatusMap() {
    return array(
      1 => 'In Quene',
      2 => 'In Progress',
      4 => 'Complete',
      5 => 'Complete',
      6 => 'Error',
    );
  }
  
  /**
   * Logger proxy setcion
   */
  public function setLogger($logger) {
    $this->APLogger->setLogger($logger);
    if($this->bp instanceof BuildPlan) {
      $this->bp->setLogger($logger_name);
    }
  }
  public function getLogger() {
    return $this->APLogger->getLogger();
  }
  /**
   * @author earnest dot berry at gmail dot com
   * @params Just a message will default to an info type msg.
   */
  private function log($type = 'info', $msg = null) {
    if($msg === null) {
      $msg = $type;
      $type = 'info';
    }

    $this->APLogger->log($type, $msg);
    return;
  }
  
  function __construct($run_id = null) {
    $this->APLogger = new APSimpleLogger();
    
    if($run_id != null) {
      $this->run_id = $run_id;
      $this->load();
    }
    
  }
  

  
  
  function __destruct() {
    $this->update();
  }
  
  
  function load() {
    $info = db_fetch_object(db_query("SELECT * FROM {ap_buildrun} WHERE ap_buildrun_id = %d", $this->run_id));
    $this->start = $info->ap_run_start;
    $this->stop = $info->ap_run_stop;
    $this->status = $info->ap_run_status;
    $this->build_name = $info->ap_run_name;
    
    // Load extra
    $extra = unserialize($info->ap_buildrun_data);
    if(is_a($extra, 'BuildPlan')) {
      $this->setBuildPlan($extra);
    }
    
    // Get the logs
    $rs = db_query("SELECT * FROM {autopilot_logger} WHERE logger = '%s'", $this->build_name);
    $logs = array();
    while($log = db_fetch_array($rs)) {
      $logs[$log['autopilot_log_id']] = $log;
    }
    $this->logs = $logs;
  }
  
  
  public static function buildRunFactory($info) {
    if(is_a($info, 'BuildPlan')) {
      $run = new BuildRun();
      $run->setLogger($info->getLoggerName());
      $run->setBuildPlan($info);
      return $run;
    }
    elseif(is_numeric($info)) {
      $run = new BuildRun($info);
    }
    else {
      return new BuildRun();
    }
  }
  
  function getName() {
    return $this->build_name;
  }
  
  function setUpdateScope($scope) {
    $this->log(t('[BuildRun] Setting update scope to %scope', array('%scope' => $scope)));
    $this->update_scope = $scope;
    // We don't set this in the BP because we may not have the oobject init yet
  }

  function setCacheRebuild($type, $value) {
    $this->cache_rebuild[$type] = $value;
  }
  function getCacheRebuildOptions() {
    return $this->cache_rebuild;
  }
  
  function getLogs($log_page = 0, $log_legnth = null) {
    $page_logs = array();
    if($log_legnth == 0 && $log_legnth == null) {
      return $this->logs;
    }
    // Find the page
    $page_logs = array();
    $page = 1;
    if($log_legnth == null) {
      $log_legnth = count($this->logs) + 1;
    }
    foreach($this->logs as $log_id => $log) {
      if($log_id > $log_page) {
        $page_logs[] = $log;
      }
      if($page >= $log_legnth) {
        return $page_logs;
      }
    }
    return $page_logs;
  }
  
  function setBuildPlan(BuildPlan $bp) {
    $this->bp = $bp;
    $this->log(t('Set buildplan to %bp', array('%bp' => $bp->getName())));
    return;
  }
  function getBuildPlan() {
    return $this->bp;
  }
  function setBuildName($name) {
    $this->build_name = $name;
  }
  function getBuildName() {
    return $this->build_name;
  }
  
  function setLockStep($lock) {
    $this->lock_step = $lock;
  }
  /**
   * Initializes information
   */
  public function init() {
    if($this->run_id <= 0 && !is_numeric($this->run_id)) {
      $run_id = db_next_id('{ap_buildrun}_ap_buildrun_id');
      $bp = $this->bp;
      $try = db_query("INSERT INTO {ap_buildrun}(ap_buildrun_id, bpid, ap_run_name) VALUES(%d, %d, '%s');", $run_id, $bp->getID(), $this->getName());
      if(!$try) {
        db_query("DELETE FROM {ap_buildrun} WHERE ap_buildrun_id = %d", $run_id);
        $this->runnable = false;
        $this->initialize = true;
        return false;
      }
      $this->run_id = $run_id;
    }
    $this->initialize = true;
    $this->runnable = true;
  }
  
  function setTempArea($path) {
    $this->temp_area = $path;
  }
  function getTempArea() {
    return $this->temp_area;
  }
  function runBuild() {
    $this->log('info', t("Running Build Init"));
    if(!$this->initialize) {
      $this->init();
    }
    $this->log('debug', t("Checking that build is runnable..."));
    if(!$this->runnable) {
      drupal_set_message(t('This is NOT runnable.'), 'error');
      $this->log('fatal', t('Build found to be un-runnable.'));
      return false;
    }
    $this->log('debug', t("Build RUNNABLE OK!"));
    $bp = $this->bp;
    $bp->setUpdateScope($this->update_scope);
    foreach($this->cache_rebuild as $type => $value) {
      $bp->setCacheRebuild($type, $value);
    }
    $this->status = BUILDRUN_IN_PROGRESS;
    $this->log('debug', t("Updating run status to " . $this->status));
    $this->update();
    $this->log('debug', t("BuildRun is starting run..."));
    $this->start();
    $this->update();
    
    try {
      //sleep(10);
      $this->setBuildName($this->build_name);
      $bp->runFastBuild($this);
    }
    catch (Exception $ex) {
      $this->status = BUILDRUN_COMPLETE;
      $this->stop();
      $this->update();
      drupal_set_message(t('There was an error during the build run.'), 'error');
      return false;
    }
    $this->status = BUILDRUN_COMPLETE;
    $this->stop();
    $this->update();
    return true;
  }
  
  /**
   *
   * This starts and setups up the run.
   * The init function setps up our database.
   * The start function setups up our logger, and
   * tells the neccessary objects that we are
   * currently in a run.
   *
   */
  function start() {
    $this->start = time();
  }
  
  function stop() {
    $this->stop = time();
  }
  
  /**
   * Updates the run information ONLY if we are
   * currelying running a build.
   */
  public function update() {
    $extra = array();
    //$bp = clone $this->bp;
    //$send_item = serialize($bp);
    $try = db_query("UPDATE {ap_buildrun}
             SET ap_run_start = %d, ap_run_stop = %d,
             ap_run_status = %d, ap_buildrun_data = '%s'
             WHERE ap_buildrun_id = %d",
             $this->getRunStart(), $this->getRunStop(),
             $this->getRunStatus(), serialize($this->bp),
             $this->getID());
    
    if(!$try) {
      throw new Error('Erorr while updaint the build run.');
    }
    return $try;
  }
  
  public function getID() {
    return $this->run_id;
  }
  public function getRunStart() {
    return $this->start;
  }
  public function getRunStop() {
    return $this->stop;
  }
  public function getRunStatus() {
    return $this->status;
  }
  public function getTotalTime() {
    $opt = array();
    $opt['to'] = $this->getRunStart();
    $opt['precision'] = 'second';
    $opt['parts'] = 2;
    $opt['distance'] = false;
    return _autopilot_time_diff($this->getRunStop(), $opt);
  }
  
  
  /**
   * Function to look up different buildRunStatus
   */
  public static function buildRunStatus($lookup) {
    switch($lookup) {
      case 1:
        $status = t('BUILD RUN IN QUEUE');
        break;
      case 2:
        $status = t('IN PROGRESS');
        break;
      case 4:
        $status = t('BUILDRUN COMPLETE');
        break;
    }
    
    return $status;
  }
  
  /**
   * This function backups up the current build
   * TODO: Finish implementing
   */
  public function backupBuild() {
    // Run a backup of the current build
    // Backup the web target
    return;
    $bp = $this->getBuildPlan();
    $ct = $bp->getCodeTarget();
    $ct->backup();
    $st = $bp->getSQLTarget();
    $st->backup();
  }
  
}

