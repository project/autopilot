<?php


class Channel {
  private $key_chain = null;
  private $channel_info = null;
  private $os_type = null;
  private $channel_open = false;
  
  function __construct() {
    $this->os_type = _autopilot_detect_os();
    return;
  }
  
  /**
   * Factory that returns the class name for the spcified channel type, or
   * for the specified channel ID.
   *
   * @params $cid int The ID of the channel
   * @params $type string The type of channel factory we want.
   *
   * @return mixed The class name if found, or false if not.
   */
  public static function channelFactory($cid = null, $type = null) {
    $class_name = 'Channel';
    if($cid == null && $type != null) {
      $class_name = "Channel" . strtoupper($type);
      if(class_exists($classname)) {
        return $class_name;
      }
      else {
        return false;
      }
    }
    else if($cid != null && is_numeric($cid)) {
      if($type == null) {
        // TODO: 
        // Search the db, and grab the type using the ID
      }
      $class_name = Channel::channelFactory(null, $type);
      $channel = new $class_name($cid);
      return $channel;
    }
    return $class_name;
  }
  
  /**
   * Returns a list of currently implemented channels.
   */
  public static function channelTypes() {
    static $channel_list;
    if($channel_list) {
      return $chanel_list;
    }
    
    $classes = get_declared_classes();
    foreach($classes as $k => $classname) {
      if(get_parent_class($classname) == 'Channel') {
        $class = new $classname();
        $channel_list[$classname] = $class->channelName();
        $class = null;
      }
    }
    
    return $channel_list;
  }
  
  function isOpen() {
    return $this->channel_open;
  }
  
  /**
   * Prepares and opens this channel's
   * connection.
   */
  function openChannel() {
    return;  
  }
  
  function closeChannel() {
    return;
  }
  
  /**
   * Copies the data from path
   * to a path on the remote end of the channel.
   */
  function copy($from_path, $to_path = '.') {
    return;
  }
  
  /**
   * Getters and setters
   */
  function setKeyChain(KeyChain $kc) {
    $this->key_chain = $kc;
    return;
  }
  
  function getKeyChain() {
    return $this->key_chain;
  }
  
}
