<?php
/**
 *  STAGES
 */
define('AUTOPILOT_BUILDPLAN_STAGE_NOTHING', null);
define('AUTOPILOT_BUILDPLAN_STAGE_BUILDING', 1);
define('AUTOPILOT_BUILDPLAN_STAGE_POSTBUILD', 2);
define('AUTOPILOT_BUILDPLAN_STAGE_CLEANUP', 3);
define('AUTOPILOT_BUILDPLAN_STAGE_COMPLETE', 4);
  
class SimpleBuildPlan extends BuildPlan {

  
  // General Varibles
  private $codeBuild = null;
  private $sqlBuild = null;
  private $codeTarget = null;
  private $sqlTarget = null;
  private $logger = null;
  private $update_scope = 'full';
  private $run_remote = true;
  private $tmp_area = null;
  private $cache_rebuild = array();
  
  function __construct($bpid = null) {
    parent::__construct($bpid);
    $this->logger = LoggerManager::getLogger($this->getName());
    if($bpid) {
      $this->loadSimplePlan($bpid);
    }
  }
  
  function runRemote() {
    $this->run_remote = true;
  }
  function runLocal() {
    $this->run_remote = false;
  }
  
  function loadSimplePlan($bpid) {
    $info = db_fetch_object(db_query("SELECT * FROM {ap_build_plan} WHERE bpid = %d", $bpid));
    // Load the code build
    $cbuild = Build::buildFactory($info->code_build_bid);
    // SQL Build
    $sbuild = Build::buildFactory($info->sql_build_bid);
    // Code Target
    $ctarget = Target::targetFactory($info->code_target_tid);
    if(is_numeric($info->code_target_kcid) && $info->code_target_kcid > 0) {
      $ctarget->setKeyChain(new PontoKeyChain($info->code_target_kcid));
    }
    // SQL Target
    $starget = Target::targetFactory($info->sql_target_tid);
    if(is_numeric($info->sql_target_kcid)  && $info->sql_target_kcid > 0) {
      $starget->setKeyChain(new PontoKeyChain($info->sql_target_kcid));
    }
    // If we have a code build, lets try everyting else
    if($cbuild) {
      $this->setCodeBuild($cbuild);
      $this->setSQLBuild($sbuild);
      $this->setCodeTarget($ctarget);
      $this->setSQLTarget($starget);
    }
    return;
  }
  
  function setCodeBuild(Build $build) {
    $this->codeBuild = $build;
  }
  function getCodeBuild() {
    return $this->codeBuild;
  }
  function setSQLBuild(Build $build) {
    $this->sqlBuild = $build;
  }
  function getSQLBuild() {
    return $this->sqlBuild;
  }
  
  function setCodeTarget(Target $target) {
    $this->codeTarget = $target;
  }
  function getCodeTarget() {
    return $this->codeTarget;
  }
  function setSQLTarget(Target $target) {
    $this->sqlTarget = $target;
  }
  function getSQLTarget() {
    return $this->sqlTarget;
  }
  
  function setCodeRevision($rev) {
    $build = $this->codeBuild;
    if(!$build) {
      throw new Exception('Attempt to set a revsion when a build was not set.');
    }
    $build->setRevision($rev);
    $this->codeBuild = $build;
    $build = null;
    $this->codeRevision = $rev;
    return;
  }
  function getCodeRevision() {
    return $this->codeRevision;
  }
  function setSQLRevision($rev) {
    $build = $this->sqlBuild;
    $build->setRevision($rev);
    $this->sqlBuild = $build;
    $build = null;
    $this->sqlRevision = $rev;
    return;
  }
  function getSQLRevision() {
    return $this->sqlRevision;
  }
  
  function setUpdateScope($scope) {
    $this->log(t('Setting the update scope to %scope', array('%scope' => $scope)));
    $this->update_scope = $scope;
  }
  
  function setBuildRunRef(BuildRun $buildRun) {
    $this->buildRun = $buildRun;
  }
  
  function setTempArea($tmp_path) {
    $this->tmp_area = $tmp_path;
  }
  function getTempArea() {
    return $this->tmp_area;
  }

  function setCacheRebuild($type, $value) {
    $this->cache_rebuild[$type] = $value;
  }
  function getCacheRebuild() {
    return $this->cache_rebuild;
  }
  function getCacheRebuildOption($type) {
    return $this->cache_rebuild[$type];
  }
  
  /**
   * Runs the build for this setup
   * TODO: This should be in the BuildRun class instead.
   */
  function runFastBuild(BuildRun $br) {
    
    $cbuild =& $this->codeBuild;
    $ctarget =& $this->codeTarget;
    $sbuild =& $this->sqlBuild;
    $starget =& $this->sqlTarget;
    
    
    // ---- BEGINE DEBUG
    $build_folder_name = $br->getBuildName();
    if($build_folder_name == null) {
      $build_folder_name = 'build_' . time();
      $br->setBuildName($build_folder_name);
    }
    $logger_name = $build_folder_name;
    // TODO: This is set in a parent level. Remove that. or merge.
    $this->setLogger($logger_name);
    $cbuild->setLogger($logger_name);
    $ctarget->setLogger($logger_name);
    $sbuild->setLogger($logger_name);
    $starget->setLogger($logger_name);
    
    $tmp_area = variable_get('autopilot_tmp_path', file_directory_temp()) . '/' . $build_folder_name;
    $this->setTempArea($tmp_area);
    
    umask(0000); // Set the Mask BACK!
    mkdir($tmp_area, 0755, true);
    // ---- END DEBUG
    
    // Setup the Run Reference
    $br->setTempArea($tmp_area);
    
    try {
      
      /**
       * This section deals with the HTML code
       */
      
      $this->log('info', t('Initializing code checkout from repository...'));
      // If remote, then we need to run the ssh command to checkout
      
      if( $this->run_remote != true ) {
        $cbuild->checkout($tmp_area . '/html');
        $ctarget->writeDir($tmp_area, $tmp_area . '/html', $ctarget->getBuildPath() . '/' . $build_folder_name, true, true); // Pushes the code up
      }
      # Checkout Remotley by running a script
      else {
        $this->log('info', t('Checking out code remotley.'));
        $script_commands = array();
        $script_commands[] = $cbuild->checkout($ctarget->getBuildPath() . '/' . $build_folder_name . '/html', true, true);
        
        $cmd = implode(" && ", $script_commands);
        $this->log('debug', t('Running command: %cmd', array('%cmd' => $cmd)));
        $output = $ctarget->runRemoteCommand($cmd);
        BuildSVN::validateRawOutput($output); // Will throw an error if invalid
      }
      $this->log('info', t('Code checkout complete.'));
      
      // Run Feature Sets1yy
      // DEBUG Ignoring feature sets
      foreach( $this->getEnabledFeatureSets() as $fsid => $val) {
        $fs = null;
        $fs = new FeatureSet($fsid);
        $fs->setRemoteBuildPath($ctarget->getBuildPath() . '/' . $build_folder_name . '/html');
        $feature_set_script = $fs->execute($br, $ctarget, $this->run_remote);
        if(is_array($feature_set_script)) {
          $fs_script .= implode("\n", $feature_set_script);
        }
      }
      
      if( $this->run_remote ) {
        // Write the file
        $local_path = file_directory_temp() . '/' . $build_folder_name . '/fs_cmds.sh';
        $remote_path = "/tmp/$build_folder_name/fs_cmds.sh";
        file_put_contents($local_path, $fs_script);
        $ctarget->makePath("/tmp/$build_folder_name"); // Make sure the folder exists
        $ctarget->copyFileToTarget($local_path, $remote_path);
        $ctarget->runRemoteCommand("sh $remote_path");
      }
      
      /*********************************
       *
       * Database Update/Push Section
       * 
       *********************************
       */
      if($this->update_scope != 'code') {
        // Push the sql up
        $this->log('info', t('Initializing SQL checkout from repository...'));
        $sql_file_info = pathinfo($sbuild->getRepoPath());
        $sql_file_name = $sql_file_info['basename'];
        $sql_script = $sbuild->checkout($starget->getBuildPath() . '/' . $build_folder_name . '/sql/' . $sql_file_name, true, true);

        if( $this->run_remote ) {
          // Write the file
          $local_path = file_directory_temp() . '/' . $build_folder_name . '/sql_cmds.sh';
          $remote_path = "/tmp/$build_folder_name/sql_cmds.sh";
          file_put_contents($local_path, $sql_script);
          $starget->makePath("/tmp/$build_folder_name"); // Make sure the folder exists
          $starget->makePath($starget->getBuildPath() . '/' . $build_folder_name . '/sql'); // Make sure the export folder exists
          $starget->copyFileToTarget($local_path, $remote_path);
          //$starget->runRemoteCommand("sh $remote_path");
          $starget->runRemoteCommand($sql_script);
        }
        //$sbuild->checkout($tmp_area . '\sql');
        $this->log('info', t("SQL Checkout complete"));
        $this->log('info', t("Pushing SQL to target..."));
        $this->log('info', t("SQL Pushed to target OK"));
        
        // See if we were given the full path
        $path = pathinfo($sbuild->getFullRepoPath());
        if(isset($path['extension'])) {
          $starget->setRemoteFilePath($starget->getBuildPath() . '/' . $build_folder_name . '/sql/' . $path['basename']);
        }
        else {
          # Else Assume site.sql
          $starget->setRemoteFilePath($starget->getBuildPath() . '/' . $build_folder_name . '/sql/site.sql'); // THIS IS THE RELATIVE PATH TO THE BUILD AREA /sql
        }
      
        $starget->importDatabase(); // Commented out for debuggin

        // Now run the feature sets again to give them a chance to mess with the database.
        // Run Feature Sets1yy
        // DEBUG Ignoring feature sets
        /* 
        foreach( $this->getEnabledFeatureSets() as $fsid => $val) {
          $fs = null;
          $fs = new FeatureSet($fsid);
          $fs->setRemoteBuildPath($starget->getBuildPath() . '/' . $build_folder_name . '/sql');
          $feature_set_script = $fs->execute($br, $starget, $this->run_remote);
          if(is_array($feature_set_script)) {
            $fs_script .= implode("\n", $feature_set_script);
          }
        }
        if( $this->run_remote ) {
          // Write the file
          $file_name = uniqid('fs_cmds_') . '.sh';
          $local_path = file_directory_temp() . '/' . $build_folder_name . '/' . $file_name;
          $remote_path = "/tmp/$build_folder_name/$file_name";
          file_put_contents($local_path, $fs_script);
          $starget->makePath("/tmp/$build_folder_name"); // Make sure the folder exists
          $starget->copyFileToTarget($local_path, $remote_path);
          $starget->runRemoteCommand("sh $remote_path");
        }
        */
        # Rebuild the cache if neccessary
        
        if( $this->getCacheRebuildOption('module') == true) {
          $this->log('info', t('Attempting to rebuild the cache'));
          $fs = new FeatureSetDrupalCommands();
          $code_lines = array();
          $code_lines[] = 'module_rebuild_cache();';
          $code_lines[] = 'system_theme_data();';
          $code = implode("\n", $code_lines);
          $this->log('debug', t('Setting Drupal cache rebuild code...'));
          $fs->setDrupalCode($code);
          $this->log('debug', t('Setting Drupal domain to %domain', array('%domain' => $ctarget->getAddress())));
          $fs->setDrupalDomain($ctarget->getAddress());
          $feature_set_script = $fs->execute($br, $ctarget, $this->run_remote);
          if(is_array($feature_set_script)) {
            $fs_script .= implode("\n", $feature_set_script);
          }
          // Now run the code
          $file_name = uniqid('fs_cmds_') . '.sh';
          $local_path = file_directory_temp() . '/' . $build_folder_name . '/' . $file_name;
          $remote_path = "/tmp/$build_folder_name/$file_name";
          file_put_contents($local_path, $fs_script);
          $ctarget->makePath("/tmp/$build_folder_name"); // Make sure the folder exists
          $ctarget->copyFileToTarget($local_path, $remote_path);
          $ctarget->runRemoteCommand("sh $remote_path");
        }
        
        
      }
      
      // Now commit everything by linking to the build for the html
      $ctarget->setWebCurrentBuild($build_folder_name); // Sets the build to the this build folder via symlink
    
    }
    catch ( Exception $e ) {
      // TODO: Use the logger to log errors and throw an error!
      drupal_set_message(t('An exception was found during the build.'), 'error');
      drupal_set_message($e->getMessage(), 'error');
      throw $e; // Throw the excpetion away from us
    }
    // Clean up
    @unlink($tmp_area);
  }
  
  /**
   * Returns the current stage that we are in.
   * The stages are:
   * - NULL This means we are not building and nothing is happening
   * - BUILD This means we are gathering code and other information and pushing to remote systems.
   * - POST_BUILD The information has been pushed to the remote systems, and we are doing
   * post build items, which could include pushing up individual artifiacts and other misc. tasks.
   * - CLEAN_UP We are deallocating resources we used.
   */
  public function getCurrentStage() {
  }
  public function setCurrentStage($stage) {
    $this->current_stage = $stage;
  }
  
  /**
   * Major fnction that compiles this
   * BuildPlan into a PhingFile to be run.
   */
  public function compilePhing() {

    $cbuild = $this->codeBuild;
    $ctarget = $this->codeTarget;
    $sbuild = $this->sqlBuild;
    $starget = $this->sqlTarget;
    
    $logger = $this->logger; // Better way to do logging
    
    // ---- BEGINE DEBUG
    if($build_folder_name == null) {
      $build_folder_name = 'build_' . time();
    }
    $tmp_area = variable_get('autopilot_tmp_path', file_directory_temp()) . '/' . $build_folder_name;
    $mask = umask();
    umask(0000); // Set the Mask BACK!
    mkdir($tmp_area, 0777, true);
    umask($mask);
    // ---- END DEBUG
    
    try {
      
      /**
       * This section deals with the HTML code
       */
      $logger->info("Initializing code checkout from repository...");
      // PHING CODE CHECKOUT SECTION
      $cbuild->checkout($tmp_area . '/html');
      // PHING CODE PUSH TO SECTION
      $ctarget->writeDir($tmp_area, $tmp_area . '/html', $ctarget->getBuildPath() . '/' . $build_folder_name, true, true); // Pushes the code up
      // PHING LOG OUTPUT/ECHO
      $logger->info("Code checkout complete");
      
      
      if($this->update_scope != 'code') {
        /**
         * This section deals with the SQL code
         */
        // Push the sql up
        $logger->info("Initializing SQL checkout from repository...");
        // PHING SQL CHECKOUT SECTION
        $sbuild->checkout($tmp_area . '\sql');
        $logger->info("SQL Checkout complete");
        $logger->info("Pushing SQL to target...");
        // PHING SQL PUSH SECTION
        $starget->writeDir($tmp_area, $tmp_area . '\sql', $starget->getBuildPath() . '/' . $build_folder_name, true, true);
        $logger->info("SQL Pushed to target OK");
        
        
        // See if we were given the full path
        $path = pathinfo($sbuild->getFullRepoPath());
        if(isset($path['extension'])) {
          $starget->setRemoteFilePath($starget->getBuildPath() . '/' . $build_folder_name . '/sql/' . $path['basename']);
        }
        else {
          # Else Assume site.sql
          $starget->setRemoteFilePath($starget->getBuildPath() . '/' . $build_folder_name . '/sql/site.sql'); // THIS IS THE RELATIVE PATH TO THE BUILD AREA /sql
        }
        // PHING IMPORT DATABASE ON TARGET
        $starget->importDatabase(); // Commented out for debuggin
      }
      
      // Now commit everything by linking to the build for the html
      // PHING RUN CUSTOM REMOTE COMMAND
      $ctarget->setWebCurrentBuild($build_folder_name); // Sets the build to the this build folder via symlink
    
    }
    catch ( Exception $e ) {
      // TODO: Use the logger to log errors and throw an error!
      throw $e; // Throw the excpetion away from us
    }
    // Clean up
    @unlink($tmp_area);
  }
    
  public function save() {
    // Use the parent save option
    $try = parent::save();
    if(!$try) {
      return $try;
    }
    // Update with our data
    $bpid = $this->getID();

    // Becuase PHP does NOT dereference its
    // variables correctly just yet, we
    // must list them all out here.
    $cb = $this->codeBuild;
    $cbk = $cb->getKeyChain();
    $sb = $this->sqlBuild;
    $sbk = $sb->getKeyChain();
    $ct = $this->codeTarget;
    $ctk = $ct->getKeyChain();
    $st = $this->sqlTarget;
    $stk = $st->getKeyChain();
    
    $code_build_bid = $cb->getID();
    if($cbk) {
      $code_build_kcid = $cbk->getID();
    }
    $sql_build_bid = $sb->getID();
    if($sbk) {
      $sql_build_kcid = $sbk->getID();
    }
    $code_target_tid = $ct->getID();
    if($ctk) {
      $code_target_kcid = $ctk->getID();
    }
    $sql_target_tid = $st->getID();
    if($stk) {
      $sql_target_kcid = $stk->getID();
    }
    
    $try = db_query("UPDATE {ap_build_plan}
           SET 
          code_build_bid = '%d', code_build_kcid = '%d',
          sql_build_bid = '%d', sql_build_kcid = '%d',
          code_target_tid = '%d', code_target_kcid = '%d',
          sql_target_tid = '%d', sql_target_kcid = '%d' 
          WHERE bpid = %d",
          $code_build_bid, $code_build_kcid,
          $sql_build_bid, $sql_build_kcid,
          $code_target_tid, $code_target_kcid,
          $sql_target_tid, $sql_target_kcid, 
          $bpid);
    return $try;
  }
  
}

