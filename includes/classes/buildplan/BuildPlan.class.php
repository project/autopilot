<?php

class BuildPlan implements APLoggableInterface {
  
  private $bpid = null;
  private $bpinfo = null;
  private $bptitle = null;
  private $targets = array();
  private $builds = array();
  private $error_stack = array();
  private $buildplan_steps = array();
  private $is_running = false;
  private $temp_path = '';
  private $autopilot = null;
  private $APLogger = null;
  
  private $plan_widgets = array();
  
  private $enabled_featuresets = array();
  
  protected $create_new = false;
  
  /**
   * Logger proxy setcion
   */
  public function setLogger($logger) {
    $this->APLogger->setLogger($logger);
  }
  public function getLogger() {
    return $this->APLogger->getLogger();
  }
  public function getLoggerName() {
    return $this->APLogger->getLoggerName();
  }
  /**
   * @author earnest dot berry at gmail dot com
   * @params Just a message will default to an info type msg.
   */
  public function log($type = 'info', $msg = null) {
    if($msg === null) {
      $msg = $type;
      $type = 'info';
    }
    $this->APLogger->log($type, $msg);
    return;
  }
  
  /**
   *
   * @abstract Constructor to load a build plan.
   *
   * @params int $bpid The Build Plan id to load
   *
   * @return void
   */
  function __construct($bpid = null) {
    $this->APLogger = new APSimpleLogger();
    
    if (!is_numeric($bpid)) {
      $this->bpinfo = new stdClass();
    }
    else {
      // Load the build plan by gather
      // the required information.
      $this->load($bpid);
    }
    $this->autopilot = AutoPilot::setCurrentAutoPilot();
    
    return;
  }
  
  /**
   *
   * If we have a temp directory open, we want to clean up after
   * our selves.
   */
  function __destruct() {
    if($this->temp_path) {
      if(_autopilot_detect_os() == AUTOPILOT_OSTYPE_WINDOWS) {
        @exec('rmdir /S /Q "' . $this->temp_path . '"');
      }
      else {
        exec('rm -rf ' . $this->temp_path);
      }
    }
    return;
  }
  
  /**
   *
   * @abstract This function loads the Build Plan information, and is
   * usually called by the constructor.
   * This can also be called to "reload" the Build Plan.
   *
   * @params $bpid int Build Plan id
   *
   * @return void
   */
  private function load($bpid) {
    
    # General Info
    $bpinfo = db_fetch_object(db_query("SELECT * FROM {ap_build_plan} 
                                       WHERE bpid = %d", $bpid));
    $this->bpinfo = $bpinfo;

    # Builds
    $brs = db_query("SELECT * FROM {ap_bp_builds} WHERE bpid = %d", $bpid);
    while($row = db_fetch_object($brs)) {
      $this->builds[$row->bpid] = $row->bpid;
    }
    
    # Targets
    $trs = db_query("SELECT * FROM {ap_bp_targets} WHERE bpid = %d", $bpid);
    while($row = db_fetch_object($brs)) {
      $this->targets[$row->tid] = $row->tid;
    }
    
    # Commands
    $crs = db_query("SELECT * FROM {ap_bp_targets} WHERE bpid = %d", $bpid);
    while($row = db_fetch_object($crs)) {
      $this->commands[$row->cid] = $row->cid;
    }
    
    # Steps
    $srs = db_query("SELECT * FROM {ap_build_plan_steps} WHERE bpid = %d ORDER BY step_number ASC", $bpid);
    while($row = db_fetch_array($srs)) {
      $this->buildplan_steps[$row['sid']] = $row;
    }
  }
  
  /**
   *
   * @abstract This actually runs the FULL build plan.
   * This is done by starting with the first item in the plan,
   * and executing the item.
   * What the class is mainly for is controlling and watching each
   * step in the build plan. This is basically the "controller"
   * for the build plan.
   * The information about the run can be referenced through the function
   * - getRunOutput()
   * - getRunLog()
   * - getRunErrors()
   * - getLastRunID()
   * 
   *
   * @param void
   *
   * @return boolean True/False to tell if the full plan ran ok.
   * 
   */
  public function runBuildPlan() {
    // Loop through the build plan array
    $buildplan_steps = $this->getSteps();
    try {
      foreach($buildplan_steps as $sid => $bstep) {
        // Now we can load the step up, and tell it to execute
        $step = null;
        $step = new BuildStep($bstep['sid']);
        // Set the BuildPlan reference
        $step->setBuildPlanRef($this);
        $step->execute(); // Execute the step
      }
    }
    catch(Exception $e) {
      drupal_set_message(t('There was an error running the build.'), 'error');
      drupal_set_message($e->getMessage(), 'error');
    }
  
    return;
  }
  # Build area
  
  /**
   * Creates a temp area for the build plan to work in.
   *
   * @param boolean $new Set to true if you do NOT want the current temp area
   * and you wish for the function to create a new temp area.
   *
   * @return String Full path to the temp area.
   */
  public function getTempArea($new = false) {
    static $temp_path;
    if($new == true || !isset($temp_path)) {
      $temp_path = variable_get('autopilot_tmp_path', file_directory_temp()) . '/' . time(); 
      if(!mkdir($temp_path)) {
        throw new Exception(t('Unable to create working area.'));
      }
      else {
        $this->temp_path = $temp_path;
      }
    }
    return $temp_path;
  }
  
  
  /**
   * TODO: This pattern-creating a skeleton, and then updating-is depracated
   * and not do able in D6 do to db_next_id is gone.
   * 
   * @abstract Saves the current build plan
   * 
   * @return boolean True/False depending on if the save was
   * sucessful.
   */
  public function save() {
    // Run the save function of the other classes (builds, targets, commands)
    // Save the build first
    global $user;
    # Insert the new build
    if($this->bpid == null && $this->create_new) {
      $bpid = db_next_id('{ap_build_plan}_bpid');
      $try = db_query("INSERT INTO {ap_build_plan}(bpid, uid) VALUES(%d, %d)", $bpid, $user->uid);
      $try = $try && db_query("INSERT INTO {ap_build_plans}(apid, bpid) VALUES(%d, %d)", AutoPilot::getCurrentAutoPilotID(), $bpid);
      if(!$try) {
        db_query("DELETE FROM {ap_build_plan} WHERE bpid = %d", $bpid);
        db_query("DELETE FROM {ap_build_plans} WHERE bpid = %d", $bpid);
        throw new Exception('Unable to save the Build Plan for ' . $bpid);
      }
      $this->bpid = $bpid;
      $this->bpinfo->bpid = $bpid;
    }
    # Update this current build
    $bpinfo = $this->bpinfo;

    $try = db_query("UPDATE {ap_build_plan}
            SET bp_name = '%s', bp_description = '%s',
            bp_ownership = %d, lid = %d
            WHERE bpid = %d",
            $bpinfo->bp_name, $bpinfo->bp_description,
            $bpinfo->bp_ownership, $bpinfo->lid,
            $bpid);
    return $try;
  }
  
  function addToAutoPilot() {
    return db_query("INSERT INTO {ap_build_plans}(apid, bpid) VALUES(%d, %d)", $this->autopilot->getID(), $this->bpid);
  }
  function removeFromAutoPilot() {
    return db_query("DELETE FROM {ap_build_plans} WHERE apid = %d AND bpid = %d", $this->autopilot->getID(), $this->bpid);
  }
  /**
   *
   * Getters and setter section
   *
   */
  public function createNew() {
    $this->create_new = true;
  }
  public function getID() {
    return $this->bpinfo->bpid;
  }
  
  # Title
  public function getName() {
    return $this->getTitle();
  }
  public function setName($name) {
    return $this->setTitle($name);
  }
  public function getTitle() {
    return $this->bpinfo->bp_name;
  }
  public function setTitle($title) {
    $this->bpinfo->bp_name = $title;
    return;
  }
  
  #Label
  public function getLabelID() {
    return $this->bpinfo->lid;
  }
  public function setLabelID($lid) {
    $this->bpinfo->lid = $lid;
  }
  
  # Description
  public function getDescription() {
    return $this->bpinfo->bp_description;
  }
  
  public function setDescription($description) {
    $this->bpinfo->bp_description = $description;
    return;
  }
  
  # Ownership
  public function getOwnership() {
    return $this->bpinfo->bp_ownership;
  }
  
  public function setOwnership($ownership) {
    $this->bpinfo->bp_ownership = $ownership;
  }
  
  
  # Fire the BuildPlan hook/function
  public function registerWidget($id, $widget) {
    $this->widget[$id] = $widget;
  }
  
  public function setEnabledFeatureSet($fsid) {
    $this->enabled_featuresets[$fsid] = true;
  }
  public function removeEnabledFeatureSet($fsid) {
    unset($this->enabled_featuresets[$fsid]);
  }
  public function getEnabledFeatureSets() {
    return $this->enabled_featuresets;
  }
  
  
  // Serialization of the BuildPlan to avoid serializing the logger link
  public function __sleep() {
    return array(
      'bpid',
      'bpinfo',
      'bptitle',
      'targets',
      'builds',
      'error_stack',
      'buildplan_steps',
      'is_running',
      'temp_path',
      'autopilot',
      'plan_widgets',
      'enabled_featuresets',
      'create_new',
      );
  }
  
}
