<?php

/**
 *
 * This is the mail Build class that gets extended by the different
 * build and repository options.
 *
 */
class Build implements APLoggableInterface {
  protected $rpid = null; // Repoistory ID
  protected $bid = null;
  protected $crepo = null;
  protected $build_info = null;
  protected $build_name = '';
  protected $build_description = '';
  protected $build_path = '';
  protected $handler = '';
  protected $create_new = false;
  protected $build_ownership = AUTOPILOT_OWNERSHIP_NORMAL;
  protected $user = null;
  protected $working_revision = ''; // EMPTY Assumes HEAD
  protected $keyChain = null;
  protected $ap_type;

  // We will use the default logger
  private $BuildLogger = '';
  private $autopilot;

  // Execution vars needed
  private $build_plan_ref = null;
  private $run_id = null;


  # Empty constructor for now
  public function __construct($bid = null) {
    global $user;
    $this->user = $user;

    if($bid != null) {
      $this->load($bid);
    }
    // Debug info that the AutoPilot set was null/empty
    $this->autopilot = AutoPilot::setCurrentAutoPilot();

    // Set the logger
    $this->BuildLogger = new APSimpleLogger();
  }

  public function setLogger($logger) {
    $this->BuildLogger->setLogger($logger);
  }
  public function getLogger() {
    return $this->BuildLogger->getLogger();
  }
  public function getLoggerName() {
    return $this->BuildLogger->getLoggerName();
  }
  /**
   * @author earnest dot berry at gmail dot com
   * @params Just a message will default to an info type msg.
   */
  public function log($type = 'info', $msg = null) {
    if($msg === null) {
      $msg = $type;
      $type = 'info';
    }
    $this->BuildLogger->log($type, $msg);
    return;
  }

  /**
   * Factory
   * Types are:
   * - svn
   * - bazaar
   * - git
   * - cvs
   */
  public static function buildFactory($bid = null, $type = null) {
    if($bid == null && $type != null) {
      $classname = 'Build' . strtoupper($type);
      if(class_exists($classname)) {
        return $classname;
      }
      else {
        return 'Build';
      }
    }
    else if($bid != null && is_numeric($bid)) {

      if($type == null) {
        // TODO:
        // Search the db, and grab the type using the ID
        $temp = db_fetch_object(db_query('SELECT rpid FROM {ap_build} WHERE bid = %d', $bid));
        $type = db_result(db_query('SELECT rp_handler FROM {ponto_vc_repos} WHERE rpid = %d', $temp->rpid));
      }
      $class_name = Build::buildFactory(null, $type);

      if( $class_name == '' || !class_exists($class_name) ) {
        return false;
      }

      $build = new $class_name($bid);

      return $build;
    }

    return false;
  }

  /**
   * Function to set the execution parameters
   *
   * @params mixed $name Takes either the name of the option or an array of options.
   * @params mixed $val The valur of the option you wish to change.
   *
   * @return void
   */
  public function setExecutionOptions($name, $val = null) {
    if(is_array($name)) {
      foreach($name as $k => $v) {
        $this->execution_options[$k] = $v;
      }
    }
    else {
      $this->execution_options[$name] = $val;
    }
    return;
  }

  private function load($bid) {
    $this->bid = $bid;
    $this->build_info = db_fetch_object(db_query('SELECT * FROM {ap_build} WHERE bid = %d', $bid));
    if($this->build_info->bid == '') {
      //throw new Exception(t('Invalid Build ID given. Build not found.'));
    }
    $this->crepo = db_fetch_object(db_query('SELECT * FROM {ponto_vc_repos} WHERE rpid = %d', $this->build_info->rpid));
    $this->rpid = $this->build_info->rpid;
    $this->name = $this->build_info->build_name;
    $this->ap_type = $this->build_info->ap_type;
    if($this->crepo->kcid > 0) {
      $this->keyChain = new PontoKeyChain($this->crepo->kcid);
    }
    return;
  }


  public function getName() {
    return $this->name;
  }

  // TODO: CHOOSE ONE!!
  public function setName($name) {
    $this->name = $name;
    $this->build_name = $name;
  }

  public function getDescription() {
    return $this->description;
  }

  public function setDescription($description) {
    $this->description = $description;
    return;
  }

  public function getHandler() {
    return $this->handler;
  }

  public function getBuildPath() {
    return $this->build_path;
  }

  public function setBuildPath($path) {
    $this->build_path = $path;
  }

  public function getBuildPlanRef() {
    return $this->build_plan_ref;
  }
  public function setBuildPlanRef($ref) {
    $this->build_plan_ref = $ref;
    return;
  }

  public function getID() {
    return $this->bid;
  }
  public function getRepoID() {
    return $this->rpid;
  }

  public function useDefaultAuth() {
    $this->buildKeyChain = null;
  }
  public function setKeyChain(KeyChain $kc) {
    $this->buildKeyChain = $kc;
  }
  public function getKeyChain() {
    return $this->buildKeyChain;
  }

  /**
   * This function is only avail. when we are
   * creating a new Build.
   * Else, we need to have checks to make sure that the
   * repos is of the same type,
   * or a way to reload the object if the repo-ref is changed.
   */
  public function setRepoRef($rpid) {
    if($this->create_new) {
      $repo = db_fetch_object(db_query('SELECT * FROM {ponto_vc_repos} WHERE rpid = %d', $rpid));
      if($repo) {
        $this->crepo = db_fetch_object(db_query('SELECT * FROM {ponto_vc_repos} WHERE rpid = %d', $rpid));
      }
      else {
        return false;
      }
    }
    return true;
  }

  public function createNew() {
    $this->create_new = true;
    $this->bid = null;
    return;
  }

  public function getAPType() {
    return $this->ap_type;
  }
  public function setAPType($type) {
    $this->ap_type = $type;
  }

  /**
   * The save function is usually extended in the specific repos class
   * as this only saves the base information for the Build (into the repository).
   *
   * // TODO If the ownership is global, we don't insert into ap_builds
   */
  public function save() {
    global $user;
    if($this->bid == null && $this->create_new) {
      $bid = db_next_id('{ap_build}_bid');
      $this->bid = $bid;
      $try = db_query("INSERT INTO {ap_build}(bid, uid, ap_type) VALUES(%d, %d, '%s')", $bid, $user->uid, $this->build_type);
      if($try == false) {
        db_query("DELETE FROM {ap_build} WHERE bid = %d", $bid);
      }
    }
    else if($this->bid == null) {
      return false;
    }
    else {
      $try = true;
    }

    if($try == false) {
      return false;
    }

    #Validations
    if(trim($this->build_name) == '') {
      $this->build_name = $this->crepo->rp_name;
    }
    if(trim($this->build_description) == '') {
      $this->build_description = $this->crepo->rp_desc;
    }
    if($this->build_type == '') {
      $this->build_type = $this->crepo->rp_handler;
    }

    # Update
    $try = db_query("UPDATE {ap_build}
             SET rpid = %d,
             build_name = '%s', build_description = '%s',
             build_path = '%s', build_ownership = %d,
             build_type = '%s', ap_type = '%s',
             uid = %d
             WHERE bid = %d",
             $this->crepo->rpid, $this->build_name, $this->build_description,
             $this->build_path, $this->build_ownership,
             $this->build_type, $this->ap_type,
             $this->user->uid,
             $this->bid);

    if(!$try && $this->create_new) {
      db_query("DELETE FROM {ap_build} WHERE bid = %d", $bid);
    }
    return $try;
  }

  public function addToAutoPilot() {
    return db_query("INSERT INTO {ap_builds} (apid, bid) VALUES(%d, %d)", $this->autopilot->getID(), $this->getID());
  }
  public function removeFromAutoPilot() {
    return db_query("DELETE FROM {ap_builds} WHERE apid = %d AND bid = %d", $this->autopilot->getID(), $this->getID());
  }

  public function setRevision($rev) {
    $this->working_revision = $rev;
  }
  public function getRevision() {
    return $this->working_revision;
  }

}

