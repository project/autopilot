<?php
// Declare the interface 'BuildCodeInterface'
interface BuildCodeInterface
{
    public function checkout($to_path = '', $export = false);
    public function getLogs($rev = null, $limit = 10);
}
