<?php
// Declare the interface 'BuildInterface'
interface BuildInterface
{
    /**
     * may need some more options on this one.
     */
    public function checkout($to_path = '', $export = false);
    
    /**
     * This should return the logs as an array in the
     * with the following template:
     *
     */
    public function getLogs($rev = null, $limit = 10);
    
    // Takes it off.
    public function commit($commit_path);
    
    public function getAPType();
    
}
