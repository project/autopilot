<?php

/**
 *
 * This class is for SVN builds. We need to override the following functions:
 * - repositoryList()
 * - export($rev = '')
 * - checkout($rev = '')
 * - getLogs($rev = '', $limit = '')
 * 
 *
 * 
 */

class BuildSVN extends Build implements BuildInterface {
  
  private $temp_build_path = null;
  private $execute_output = '';
  
  
  /**
   * Static Functions
   */
  public static function validateRawOutput($output, $logger = null) {
    if(!is_array($output)) {
      $output = array($output);
    }
    foreach($output as $line) {
      if(strstr($line, 'doesn\'t exist')) {
        throw new Exception(t('Error during SVN checkout. Check log.'));
      }
    }
    return true;
  }
  /*
   * Class instnace functions
   */
  private function load($bid) {
    parent::load($bid);
    // Now do some fanagaling of our own
  }
  
  /**
   * Does dependancy checks to see
   * that the modules it needs (e.g. svn module)
   * are installed and readily avail.
   * TODO
   */
  public function validate() {
  }
  
  public function repositoryList() {
  }
  
  /**
   * Checkout the repository to the given directory.
   *
   * @param string $to_path The path to write the files to.
   * @param boolean $export Set to true to export instead of checkout.
   * @param boolean $return_command Set to true to have the raw svn command returned but not run.
   * @param String The repository path
   * 
   * @return false
   */
  public function checkout($to_path = '', $export = false, $return_command = false, $repo_path = null) {
    // DEBUG
    
    if($repo_path == null) {
      $repo = $this->getFullRepoPath();
    }
    else {
      $repo = $this->getRepoRoot() . $repo_path;
    }
    
    // If it's a file, we only want the base root
    $path = pathinfo($repo);
    if(isset($path['extension']) && !$export) {
      $repo = $path['dirname'];
    }
    $switches = array();
    
    // Check it out
    if( $return_command ) {
      if( $export ) {
        $svn_command = 'svn export --non-interactive @username @password @repo_path @remote_path';
      }
      else {
        $svn_command = 'svn co --non-interactive @username @password @repo_path @remote_path';
      }
      $svn_info = array('@username' => '', '@password' => '', '@repo_path' => '', '@remote_path' => '/tmp');
      $svn_info['@repo_path'] = $repo;
      $svn_info['@remote_path'] = $to_path;
      
      if($this->keyChain !== null) {
        if( $this->keyChain->getUsername() != '') {
          $svn_info['@username'] = '--username ' . trim($this->keyChain->getUsername());
        }
        if( $this->keyChain->getPassword() != '') {
          $svn_info['@password'] = '--password ' . $this->keyChain->getPassword();
        }
      }
      return strtr($svn_command, $svn_info);
    }
    // TODO Check to see if this checkout failed, then fail eveything else.
    if($this->keyChain !== null) {
      if( $this->keyChain->getUsername() != '') {
        $switches['username'] = trim($this->keyChain->getUsername());
      }
      if( $this->keyChain->getPassword() != '') {
        $switches['password'] = $this->keyChain->getPassword();
      }
    }
    // TODO: Log messages about the checkout here.
    $output = _svn_checkout($repo, $to_path, $this->getRevision(), $switches, $export);
    $this->execute_output .= $output;
    $this->messages[] = t('Exported %repo to %path....', array('%repo' => $repo,
                                                               '%path' => $to_path));
    return true;
  }
  /**
   * Runs the commit command on the following path
   */
  public function commit($working_copy_path, $log_msg = '') {
    $output = _svn_commit($working_copy_path, $log_msg, $this->crepo);
    return $output;
  }
  
  /**
   * 
   * 
   * 
   * 
   */
  public function getLogs($rev = null, $limit = 10) {
  }
  
  /**
   * This is the path inside the repo
   */
  public function getRepoPath() {
    return $this->build_info->build_path;
  }
  public function getRepoID() {
    return $this->build_info->rpid;
  }
  public function getRepoRoot() {
    return $this->crepo->rp_path;
  }
  
  public function getFullRepoPath() {
    $repo_root = $this->getRepoRoot();
    if($repo_root[strlen($repo_root)] == '/') {
      $repo_root = substr($repo_root,  0, strlen($repo_root) - 1);
    }
    return $repo_root . $this->getRepoPath();
  }
  /**
   * Executes the plan.
   *
   * @return boolean Return value of the run.
   */
  public function execute() {
    drupal_set_message(t('Executing build...'));
    parent::log(t('Executing build...'));
    # For now, the default plan is to checkout to the
    # temp dir of the referenced BuildPlan
    $buildplan_ref = $this->getBuildPlanRef(); // De-reference for speed
    # Get the temp path from the build plan
    $path = $buildplan_ref->getTempArea();
    $this->temp_build_path = _autopilot_sanitize_path("$path/build_" . $this->bid);
    $this->checkout($this->temp_build_path, true);
    return;
  }
  
  /**
   * Returns the path to the area where the execution
   * function will place the contents from the
   * execution
   */
  public function getBuildExecutePath() {
    return $this->temp_build_path;
  }
  
  public function getRevisions($opt = 'short') {
    return svn_get_revisions($this->crepo->rpid);
    /*
      $args = array();
      $args['path'] = $this->getRepoPath();
      $messages = svn_pontovcapi('logs', 'svn', $this->crepo, $opt, $args);
      return $messages;
    */
  }
  

}
