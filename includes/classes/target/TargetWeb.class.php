<?php
  
class TargetWeb extends Target {
  
  public function __construct($tid = null) {
    parent::__construct($tid);
  }
  
  public function getTypeTitle() {
    return t('Web/HTTP Target Server');
  }
  
  /**
   * This returns an array of options that are
   * specific to this target.
   * The options should be in the format:
   * array(
   *  'option' => array(
   *    'name' => 'option name',
   *    'type' => 'string or list',
   *    'description' => 'descitpin about it',
   *    'current_value' => 'current value of the options',
   *    'options' => 'an array of options or null if its singe and a text field'
   *   )
   *  )
   */
  public function targetOptions(TargetWeb $target = null) {
    $options = array();
    # Web Area
    $opt = array();
    $opt['field_name'] = 'target_web_path';
    $opt['name'] = t('Web Server Path');
    $opt['type'] = 'string';
    $opt['description'] = t('The path where the web pages are served from.');
    $opt['current_value'] = $this->getWebRootPath();
    $opt['options'] = null;
    $options[] = $opt;
    
    return $options;
  }
  
  public function getWebRootPath() {  
    return $this->extra['target_web_path'];
  }
  
  public function getType() {
    return 'TargetWeb';
  }

  public function save() {
    $try = parent::save();
    if($try) {
      $try = db_query("UPDATE {ap_target} SET target_type = '%s' WHERE tid = %d",
        $this->getType(), $this->getID());
    }
    return $try;
  }
  
  /***
   *
   * Implementation area for a BuildStep for the extra information
   * that is needed when executing.
   */
  public function getExecutionOptions() {
    $options = array();
    # Web Area
    $opt = array();
    $opt['field_name'] = 'sid';
    $opt['name'] = t('Select the type that you wish to act on');
    $opt['type'] = 'string';
    $opt['description'] = t('The path where the web pages are served from.');
    $opt['current_value'] = $target ? $target->getWebRootPath() : '';
    $opt['options'] = null;
    $options[] = $opt;
    
    return $options;
  }
  
  public function setWebCurrentBuild($build_name) {
    // General setup
    $keyChain = $this->targetKeyChain;
    $username = $keyChain->getUsername();
    $pass = $keyChain->getPassword();
    $host = $this->getAddress();
    
    // Create a symlink (or junction for windows) to the build path
    $ssh_proxy = _autopilot_ssh_proxy_factory($host);
    $ssh_proxy->setKeychain($keyChain);
    
    $dir_path = $this->getWebRootPath();
    $build_path = $this->getBuildPath() . '/' . $build_name . '/html'; // Gotta add /html becuase that's where we're serving from
    $cmd = "rm $dir_path";
    $ssh_proxy->sshRunRemoteCommand($cmd, false);
    // Attempted to do in one comand, but only options are && and ||, which means onlye 1 command will be run with && if one command failes.
    $this->log('info',  "Linking to new build $build_path from $dir_path...");
    $cmd = "ln -sf $build_path $dir_path";
    $ssh_proxy->sshRunRemoteCommand($cmd, false);
    // TODO: Make sure this happends.
    $this->log('info', "Link to $build_path complete.");
    
    return;
  }
  
  /**
   * @param $working_path string The working path area
   * @param $local_path string The local path that should be the same as teh remote path
   * @param $remote_path the Remote path to push this too
   */
  public function writeDir($working_path, $local_path, $remote_path = null, $recursive = true, $compress = true) {
    $logger =& LoggerManager::getLogger('writeDir');
    $logger->info("Pushing up html directory..");    
    return parent::writeDir($working_path, $local_path, $remote_path . '/html', $recursive, $compress);
  }
  
  public function backup() {
    // Run Pre-backup processes, this could include
    // removing symlinks to the files directory, etc. or other
    // items that should not be included in the backups.
    
    // General setup
    $keyChain = $this->targetKeyChain;
    $username = $keyChain->getUsername();
    $pass = $keyChain->getPassword();
    $host = $this->getAddress();
    $ssh2_proxy = _autopilot_ssh_proxy_factory($host);
    $ssh2_proxy->setKeyChain($keyChain);
    // Create a symlink (or junction for windows) to the build path
    $dir_path = $this->getWebRootPath();
    $build_path = $this->getBuildPath() . '/' . $build_path . '/html'; // Gotta add /html becuase that's where we're serving from
    
    // Now place these in the defined path
    $cmd = "cp -r $build_path $backup_path";
    $output = $ssh2_proxy->sshRunRemoteCommand($cmd, false);
    
    return;
  }
  
  
}

