<?php

class Target implements APLoggableInterface {
  
  protected $ctarget = null; // Currently loaded target
  protected $buildplan_ref = '';
  protected $execution_options = array();
  protected $target_build_area = '';
  protected $ChannelRef = null; // If it's in caps, it means is an object or class
  protected $target_options = array();
  protected $targetKeyChain = null;
  protected $tid = '';
  protected $extra = array();
  protected $logger = '';
  protected $APLogger = null;
  private $autopilot = null;
  private $targetEnv = null;
  
  public function __construct($tid = null) {
    if($tid == null) {
      $this->ctarget = new stdClass();
    }
    else {
      $this->setTargetRef($tid);
    }
    $this->logger = LoggerManager::getLogger('Target');
    // Set the autopilot
    $this->autopilot = AutoPilot::setCurrentAutoPilot();
    
    $this->APLogger = new APSimpleLogger();
    // Keychain Reference
    $ctarget = $this->ctarget;
    if(is_numeric($ctarget->kcid) && $ctarget->kcid > 0) {
      $this->targetKeyChain = new PontoKeyChain($ctarget->kcid);
    }
    // We need to use the local settings
    // TODO: A KEYCHAIN SHOULD BE CREATED INSTEAD OF STORING WITH THE TARGET TABLE!!!
    else {
      $kc = new PontoKeyChain();
      $kc->setUsername($ctarget->target_username);
      $kc->setPassword($ctarget->target_password);
      $this->targetKeyChain = $kc;
    }
    
  }
  
  public function getID() {
    return $this->tid;
  }
  public function getTypeTitle() {
    return t('General Target Server');
  }
  public function setEnv($env) {
    $this->ctarget->target_env = $env;
  }
  public function getEnv() {
    return $this->ctarget->target_env;
  }
  public function getTempPath() {
    return $this->ctarget->target_tmp_path;
  }
  /**
   * Trying to decide if each target should
   * be a type of target, like a "Network" target,
   * or an "Local File" target. But, those are taken
   * care of by "Channels" in my opinion.
   */
  public static function targetFactory($tid = null, $classname = null) {
    // Returns a list of target class types
    if($tid === null && $classname === null) {
      $target_types = array('Target' => t('Generic Target'));
      $classes = get_declared_classes();
      
      foreach($classes as $k => $class) {
        if( is_subclass_of($class, 'Target') ) {
          $temp = null;
          $temp = new $class();
          $target_types[$class] = $temp->getTypeTitle();
        }
      }

      return $target_types;
    }

    if($tid == null && $classname != null) {
      if(class_exists($classname)) {
        return new $classname();
      }
      else {
        return new Target();
      }
    }
    else if(($tid != null && is_numeric($tid))) {
      if($classname == null) {
        // TODO: 
        // Search the db, and grab the type using the ID
        $classname = db_result(db_query('SELECT target_type FROM {ap_target} WHERE tid= %d', $tid));
      }
      
      if( !class_exists($classname) ) {
        throw new Exception('Target class [' . $classname . '] not found.');
        return false;
      }
      $target = new $classname($tid);
      return $target;
    }
    
    return false;
  }
  
  public static function getUserTargets() {
    global $user;
    $rs = db_query("SELECT * FROM {ap_target} WHERE uid = %d", $user->uid);
    $rows = array();
    while($target = db_fetch_object($rs)) {
      $rows[] = $target;
    }
    return $rows;
  }
  
  public static function getAvailiableTargets() {
    global $user;
    $rs = db_query("SELECT * FROM {ap_target}");
    $rows = array();
    while($target = db_fetch_object($rs)) {
      $rows[] = $target;
    }
    return $rows;    
  }
  
  public static function targetLabels() {
    $labels = array();
    $labels['dev'] = t('Development');
    $labels['staging'] = t('Staging');
    $labels['prod'] = t('Production');
    return $labels;
  }
  
  /**
   * @abstract Saves the current target
   * @depricated
   * 
   * @return boolean True/False depending on if the save was
   * sucessful.
   */
  public function save() {
    // Run the save function of the other classes (builds, targets, commands)
    // Save the build first
    global $user;
    
    if($this->tid == null && $this->create_new) {
      $tid = db_next_id('{ap_target}_tid');
      $try = db_query("INSERT INTO {ap_target}(tid, uid) VALUES(%d, %d)", $tid, $user->uid);
      $this->tid = $tid;
      if(!$try) {
        db_query("DELETE FROM {ap_target} WHERE tid = %d", $tid);
      }
    }
    else if($this->tid == null) {
      return false;
    }
    
    
    
    # Update this current build
    // TODO: Target type is not used i don't think
    $target_info = $this->ctarget;
    $saved = db_query("UPDATE {ap_target}
            SET target_name = '%s', target_description = '%s',
            target_wpath = '%s', target_address = '%s',
            target_ownership = '%s',
            target_username = '%s', target_password = '%s',
            target_extra = '%s', target_build_path = '%s',
            target_backup_path = '%s' , target_tmp_path = '%s', 
            target_type = '%s', target_env = '%s' 
            WHERE tid = %d",
            $target_info->target_name, $target_info->target_description,
            $target_info->target_wpath, $target_info->target_address,
            $target_info->target_ownership, $target_info->target_username,
            $target_info->target_password, serialize($this->target_options),
            $target_info->target_build_path, $target_info->target_backup_path,
            $target_info->target_tmp_path,
            $target_info->target_type, $this->getEnv(),
            $this->tid
            );
    // Update the keychian
    $saved = $saved && db_query("UPDATE {ap_target}
      SET kcid = %d WHERE tid = %d", $this->getKeychainID(), $this->tid);
    if(!$saved && $this->create_new) {
      db_query("DELETE FROM {ap_target} WHERE tid = %d", $this->tid);
    }
    return $saved;
  }
  
  public function addToAutoPilot() {
    // Adds the target to the currently set AutoPilot
    return db_query("INSERT INTO {ap_targets}(apid, tid) VALUES(%d, %d)", $this->autopilot->getID(), $this->getID());
  }
  public function removeFromAutoPilot() {
    return db_query("DELETE FROM {ap_targets} WHERE apid = %d AND tid = %d", $this->autopilot->getID(), $this->getID());
  }
  
  public function createNew() {
    $this->create_new = true;
    $this->tid = null;
    $this->ctarget = new stdClass();
    return;
  }
  
  /**
   * This is the getters and setters section of
   * the code.
   */
  # Set ref
  // TODO: Error checking to make sure the tid was valid.
  public function setTargetRef($tid) {
    $target = db_fetch_object(db_query("SELECT * FROM {ap_target} WHERE tid = %d", $tid));
    $this->ctarget = $target;
    $this->tid = $target->tid;
    $this->extra = unserialize($target->target_extra);
    // Keychain Reference
    if(is_numeric($target->kcid)) {
      $this->targetKeyChain = new PontoKeyChain($target->kcid);
    }
    return;
  }
  
  // Name
  public function getName() {
    return $this->ctarget->target_name;
  }
  public function setName($name) {
    $this->ctarget->target_name = $name;
  }
  
  // Description
  public function getDescription() {
    return $this->ctarget->target_description;
  }
  public function setDescription($description) {
    $this->ctarget->target_description = $description;
  }
  
  // Set Logger
  /**
   * Logger proxy setcion
   */
  public function setLogger($logger) {
    $this->APLogger->setLogger($logger);
  }
  public function getLogger() {
    return $this->APLogger->getLogger();
  }
  public function getLoggerName() {
    return $this->APLogger->getLoggerName();
  }
  /**
   * @author earnest dot berry at gmail dot com
   * @params Just a message will default to an info type msg.
   */
  public function log($type = 'info', $msg = null) {
    if($msg === null) {
      $msg = $type;
      $type = 'info';
    }
    $this->APLogger->log($type, $msg);
    return;
  }
  
  /**
   * Set the working path
   * TODO: Use a "getDefaultWorkingPath" to differetiate when you are
   * changing paths.
   */
    
  public function getDefalutWorkingPath() {
    return $this->ctarget->target_wpath;
  }    
  
  public function getWorkingPath() {
    return $this->ctarget->target_wpath;
  }
  public function setWorkingPath($path) {
    $this->ctarget->target_wpath = $path;
    return;
  }
  
  // Address
  public function getAddress() {
    return $this->ctarget->target_address;
  }
  public function setAddress($address) {
    $this->ctarget->target_address = $address;
    return;
  }
  
  // Ownership
  public function getOwnership() {
    return $this->ctarget->target_ownership;
  }
  public function setOwnership($ownership) {
    $this->ctarget->target_ownership = $ownership;
    return;
  }
  
  // Channels
  public function getChannel() {
    return $this->targetChannel;
  }
  public function setChannel($ch) {
    $this->ctarget->target_channel = $ch;
    return;
  }
  
  /*******
   * Implementation of AutopilotExecution interface
   */
  function setBuildPlanRef(BuildPlan $bp) {
    $this->buildplan_ref = $bp;
    return;
  }
  function setBuildPlanRefID(int $bpid) {
    $this->buildplan_ref = new BuildPlan($bpid);
  }
  function getBuildPlanRef() {
    return $this->buildplan_ref;
  }
  /**
   * Function to set the execution parameters
   *
   * @params mixed $name Takes either the name of the option or an array of options.
   * @params mixed $val The valur of the option you wish to change.
   *
   * @return void
   */
  public function setExecutionOptions($name, $val = null) {
    if(is_array($name)) {
      foreach($name as $k => $v) {
        $this->execution_options[$k] = $v;
      }
    }
    else {
      $this->execution_options[$name] = $val;
    }
    return;
  }
  
  /**
   * Writes the local folder to the remote folder/path
   * - compress zips the contents up, moves them to remotes
   * temp area, unzips there, and moves. faster transer.
   *
   * Use the mkdir command with the -p option to create the parent directories.
   * That's ONLY if the remote machine is a linux machine
   *
   * On windows the command is rmdir /S /Q dir_path
   *
   * @param String $working_path This is the path to use as a working temporary directory.
   * @param String $local_path The local path to write
   * @param String $remote_path The Remote path wehre you would like the local path to be written.
   * @param boolean $recursive Set to true if you would like the function to recurse down the directory tree of the local path.
   * @param boolean $compress Set to true if you like the inforamtion compressed before sending. This is a good idea with larger files.
   *
   * @return boolean Weather the information was successfully transfered or not.
   * 
   */
  public function writeDir($working_path, $local_path, $remote_path = null, $recursive = true, $compress = true) {
    // Genearl vars needed
    $working_path = _autopilot_sanitize_path($working_path);
    $local_path = _autopilot_sanitize_path($local_path);
    $remote_path = _autopilot_sanitize_path($remote_path);
    
    if (!ini_get('safe_mode')) {
      set_time_limit(240);
    }
    $zip_name = 'tempWebTarget' . time() . '.zip';
    $remote_zip_path = $this->getRemoteTempPath() . '/' . $zip_name;
    $remote_zip_path = _autopilot_sanitize_path($remote_zip_path);
    
    
    // Try EZZip
    $cd = getcwd();
    chdir($local_path);
    $zip = new EasyZIP();
    $zip->addDir('.', true);
    $zip->zipFile( _autopilot_sanitize_path("$working_path\\$zip_name"));
    chdir($cd);
    $zip = null;
    unset($zip);
    
    // Now SCP this up to the temp DIR
    $keyChain = $this->targetKeyChain;
    $username = $keyChain->getUsername();
    $pass = $keyChain->getPassword();
    $host = $this->getAddress();
    $ssh2_proxy = _autopilot_ssh_proxy_factory($host);
    $ssh2_proxy->setKeyChain($keyChain);
    
    # Prep the build area
    $path_info = pathinfo($remote_path);
    if(isset($path_info['extension'])) {
      $makepath = $path_info['dirname'];
    }
    else {
      $makepath = $remote_path;
    }
    $cmd = "mkdir -p " . $makepath;
    
    $ssh2_proxy->sshRunRemoteCommand($cmd, $this->APLogger->getLoggerName());
    # Push up
    $this->logger->debug("Pushing to remote path " . _autopilot_sanitize_path("\"$working_path\\$zip_name\" ==> $remote_zip_path"));
    $ssh2_proxy->scpCopyToRemote(_autopilot_sanitize_path("\"$working_path\\$zip_name\"") ,$remote_zip_path);
    $cmd = "unzip -o -d $remote_path $remote_zip_path"; // Unzip to area
    $this->logger->debug("Unzipping to: $cmd");
    $ssh2_proxy->sshRunRemoteCommand($cmd);
    
    # Clean up
    $cmd = "rm $remote_zip_path";
    // INVESTIGATE: When this line is uncommented, the second zip fails
    //@unlink("$working_path\\$zip_name");
    //SSH2::sshRunRemoteCommand($host, $username, $pass, $cmd);
    
    return;
  }
  
  
  
  // Execution
  /**
   *
   * We know that when we are "executing" for a target, we are general
   * doing some action THROUGH the assigned channel. So, what we want
   * to do here is get out channel, and push the directory to our
   * remote linux machine.
   * 
   */
  function execute() {
    // We need to place code in the build_dir

    $channel = new Channel();
    $channel->setTarget($this);
    // Need to set an AutopilotRun object to capture output and errors.
    $channel->copy($from_path, $remote_build_path);
  }
  
  /**
   *
   * This takes a build number and symlinks the target, and/or sets
   * the remote target to that build. Normally this is done
   * through a symlink to the target.
   *
   * The "setBuild" for a target means that the symlink gets changed
   * to the specified target.
   *
   */
  function setBuild($build_number) {
  }
  
  /**
   * Use instance of KeyChain as it could
   * be implemented differently.
   */
  public function getKeychainID() {
    if( $this->targetKeyChain instanceof KeyChain ) {
      $kc = $this->targetKeyChain;
      return $kc->getID();
    }
  }
  
  // TODO: Change this to return a Keychain instead.
  // We want all authentication information to be
  // encapsulated in a Keychain, either a dynamically
  // created one, or a reference to one.
  public function getAuthSettings() {
    $settings = new stdClass();
    $settings->username = $this->ctarget->target_username;
    $settings->password = $this->ctarget->target_password;
    return $settings;
  }
  public function setUsername($username) {
    $this->ctarget->target_username = $username;
    return;
  }
  public function setPassword($password) {
    $this->ctarget->target_password = $password;
    return;
  }
  
  /**
   * Path settings
   */
  /**
   * Returns the path on the remote target where
   * builds are placed.
   */
  public function getBuildPath() {
    return $this->ctarget->target_build_path;
  }
  public function setBuildPath($path) {
    $this->ctarget->target_build_path = $path;
  }
  /**
   * Returns the back path
   */
  public function getBackupPath() {
    return $this->ctarget->target_backup_path;
  }
  public function setBackupPath($path) {
    return $this->ctarget->target_backup_path = $path;
  }
  
  /**
   * Returns the tmp path
   */
  public function getRemoteTempPath() {
    return $this->ctarget->target_tmp_path;
  }
  public function setRemoteTempPath($path) {
    $this->ctarget->target_tmp_path = $path;
    return;
  }
  /**
   * Target type
   */
  public function getType() {
    return $this->ctarget->target_type;
  }
  
  /**
   * Set options
   */
  public function setOptions($options) {
    foreach($options as $opt => $v) {
      $this->target_options[$opt] = $v;
    }
    // Perhaps the reloadOptions function here?
    return;
  }
  
  // Key Chain
  public function getKeyChain() {
    return $this->targetKeyChain;
  }
  public function setKeyChain(KeyChain $kc) {
    $this->targetKeyChain = $kc;
  }
  
  /**
   * Path Settings
   */
  public function copyFileToTarget($local_path, $remote_path) {
    // General setup
    $keyChain = $this->targetKeyChain;
    $username = $keyChain->getUsername();
    $pass = $keyChain->getPassword();
    $host = $this->getAddress();
    
    // Push
    $try = SSH2::scpCopyToRemote($host, $username, $pass, $local_path, $remote_path);
    return $try;
  }
  
  public function runRemoteCommand($cmd) {
    
    // General setup
    $keyChain = $this->targetKeyChain;
    $username = $keyChain->getUsername();
    $pass = $keyChain->getPassword();
    $host = $this->getAddress();
    
    if($this->getEnv() != '') {
      $cmd = implode("\n", explode("\n", $this->getEnv())) . " && " . $cmd;
    }
    // Push
    $output = SSH2::sshRunRemoteCommand($host, $username, $pass, $cmd, false, $this->getLoggerName());
    return $output;    
  }
  
  public function makePath($makepath) {
    // General setup
    $keyChain = $this->targetKeyChain;
    $username = $keyChain->getUsername();
    $pass = $keyChain->getPassword();
    $host = $this->getAddress();
    $cmd = "mkdir -p " . $makepath;
    SSH2::sshRunRemoteCommand($host, $username, $pass, $cmd);
  }
  
}

