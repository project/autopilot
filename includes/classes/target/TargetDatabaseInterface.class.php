<?php

interface TargetDatabaseInterface {
  public function importDatabase();
  public function getSQLUser();
  public function getSQLPassword();
  public function getSQLPort();
  public function getSQLDatabaseName();
  public function backup($local_path = null);
}
