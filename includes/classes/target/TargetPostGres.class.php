<?php
  
class TargetPostGres extends Target {
  
  private $remote_sql_file = '';
  
  public function getTypeTitle() {
    return t('PostGres Target');
  }
  
  /**
   * This returns an array of options that are
   * specific to this target. This is how we
   * extend a target.
   * NOTE: A GENERAL ARRAY IS RETURNED,
   * THIS ARRAY IS ONLY SIMILAR TO THE DRUPAL
   * ONE. IT GETS TRANSLATED INTO A DRUPAL FAPI.
   * 
   * The options should be in the format:
   * array(
   *  'option' => array(
   *    'name' => 'option name',
   *    'type' => 'string or list',
   *    'description' => 'descitpin about it',
   *    'current_value' => 'current value of the options',
   *    'options' => 'an array of options or null if its singe and a text field'
   *   )
   *  )
   */
  public function targetOptions(TargetWeb $target = null) {
    $options = array();
    # PostGres Username
    $opt = array();
    $opt['field_name'] = 'target_sql_username';
    $opt['name'] = t('PostGres Username');
    $opt['type'] = 'string';
    $opt['description'] = t('Username of the PostGres User');
    $opt['current_value'] = $this->getSQLUser();
    $opt['options'] = null;
    $options[] = $opt;
    # Password
    $opt = array();
    $opt['field_name'] = 'target_sql_password';
    $opt['name'] = t('PostGres Password');
    $opt['type'] = 'string';
    $opt['description'] = t('Password of the PostGres User');
    $opt['current_value'] = $this->getSQLPassword();
    $opt['options'] = null;
    $options[] = $opt;
    # Port
    $opt = array();
    $opt['field_name'] = 'target_sql_port';
    $opt['name'] = t('Port');
    $opt['type'] = 'string';
    $opt['description'] = t('Connection port of the PostGres server');
    $opt['current_value'] = $this->getSQLPort() ? $this->getSQLPort() : '3306';
    $opt['options'] = null;
    $options[] = $opt;
    # Database name
    $opt = array();
    $opt['field_name'] = 'target_sql_database';
    $opt['name'] = t('Name of the Database');
    $opt['type'] = 'string';
    $opt['description'] = t('Database name');
    $opt['current_value'] = $this->getSQLDatabaseName();
    $opt['options'] = null;
    $options[] = $opt;
    
    return $options;
  }

  public function save() {
    $try = parent::save();
    if($try) {
      $try = db_query("UPDATE {ap_target} SET target_type = '%s' WHERE tid = %d",
              'TargetPostGres', $this->getID());
    }
    return $try;
  }
  
  public function getSQLUser() {
    return $this->extra['target_sql_username'];
  }
  public function getSQLPassword() {
    return $this->extra['target_sql_password'];
  }
  public function getSQLPort() {
    return $this->extra['target_sql_port'];
  }
  public function getSQLDatabaseName() {
    return $this->extra['target_sql_database'];
  }
  public function setRemoteFilePath($path) {
    $this->remote_sql_file = $path;
  }
  
  /**
   * Imports the database on the remote target
   * TODO: PostGres import command is different
   */
  public function importDatabase() {
    // We need to run the mysqlimport command with
    $options[] = "-u" . $this->getSQLUser();
    $options[] = "-p" . $this->getSQLPassword();
    // TODO Make it possible to include lots of little mysql/DB options
    $options = implode(' ', $options);
    
    $database_name = $this->getSQLDatabaseName();
    // SQL File Path on the remote machine
    $sql_file_path = $this->remote_sql_file;
    
    $mysql_cmd = "mysql $options $database_name < $sql_file_path";
    $keyChain = $this->targetKeyChain;
    $host = $this->getAddress();
    $username = $keyChain->getUsername();
    $pass = $keyChain->getPassword();
    SSH2::sshRunRemoteCommand($host, $username, $pass, $mysql_cmd, false);
    
    return;
  }
  
  // It needs to know that the base bath is /srv/ap_builds/build_1232131
  public function writeDir($working_path, $local_path, $remote_path = null, $recursive = true, $compress = true) {
    return parent::writeDir($working_path, $local_path, $remote_path . '/sql', $recursive, $compress);
  }
  
  /**
   * This function dumps the remote database
   * and SCP's the copy back to the "local_path"
   */
  public function backup($local_path = null) {
    // Dump
    $tmp_name = $this->getSQLDatabaseName() . "_" . time() . ".sql";
    
    // We need to run the mysqlimport command with
    $options[] = "-u" . $this->getSQLUser();
    $options[] = "-p" . $this->getSQLPassword();
    
    // TODO Make it possible to include lots of little mysql/DB options
    $options = implode(' ', $options);
    
    $database_name = $this->getSQLDatabaseName();
    $mysql_cmd = "mysqldump $options $database_name > /tmp/$tmp_name";
    $keyChain = $this->targetKeyChain;
    $host = $this->getAddress();
    $username = $keyChain->getUsername();
    $pass = $keyChain->getPassword();
    $output = SSH2::sshRunRemoteCommand($host, $username, $pass, $mysql_cmd, false);
    
    // Grab the database backup if local path was given
    if($local_path != null) {
      $local_path = str_replace(' ', '::', $local_path);
      $output = SSH2::scpPullFromRemote($host, $username, $pass, "/tmp/$tmp_name", str_replace('\\', '/',$local_path));
    }
    
    return $output;
  }
  
  
  /*
   * This function dumps the remote database
   * and SCP's the copy back to the "local_path"
   */
  protected function pullRemoteSnapShot($local_path) {
    return;
  }
  
}

