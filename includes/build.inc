<?php

/**
 * 
 * 
 */

/**
 * Main overview page for builds.
 */
function builds_page($bpid = null) {
	global $user;
	$page = '';
	
	// Dispatch
	if(arg(4) == 'edit') {
		return drupal_get_form('builds_add_build_form', arg(3));
	}
	elseif(arg(4) == 'remove') {
		$build = Build::buildFactory(arg(3));
		$try = $build->removeFromAutoPilot();
		if($try) {
			drupal_set_message(t('%name has been removed from your autopilot.',
													 array('%name' => $build->getName())));
		}
		else {
			drupal_set_message(t('There was an error trying to remove %name from your AutoPilot.',
													 array('%name' => $build->getName()),
													 'error')
												);
		}
		drupal_goto('autopilot/setup/builds');
	}
	
	if( user_access('autopilot create autopilot') ) {
		$page .= theme('ap_fieldset', t('Add New Build'), drupal_get_form('builds_add_build_form'));
	}
	if( user_access('autopilot create autopilot') ) {
		$page .= theme('ap_fieldset', t('Add Existing Build'), drupal_get_form('builds_add_existing'));
	}
	$page .= drupal_render($add_build);

	
	# List the Build Plans
	$headers = array(t('RPID'), t('Name'), t('Path'), '-');
	$rows = array();
	$row = array();

	$brs = db_query("SELECT * FROM {ap_build} apb INNER JOIN {ap_builds} apbs ON apb.bid = apbs.bid
									WHERE apbs.apid = %d", AutoPilot::getCurrentAutoPilotID());
	while($crow = db_fetch_object($brs)) {
		$row = array();
		$row[] = $crow->rpid;
		$row[] = $crow->build_name;
		$row[] = $crow->build_path;
		$links = array();
		$links[] = array('title' => t('edit'), 'href' => 'autopilot/setup/builds/' . $crow->bid . '/edit');
		$links[] = array('title' => t('remove'), 'href' => 'autopilot/setup/builds/' . $crow->bid . '/remove');
		$links[] = array('title' => t('delete'), 'href' => 'autopilot/setup/builds/' . $crow->bid . '/delete');
		$row[] = theme('links', $links);
		$rows[] = $row;
	}
	$page .= '<em>' . t('RPID (Repository ID) is given here as a reference to use in feature sets. Remember, this is the ID of the main REPOSITORY that the Build is based off of, and IS NOT the actual BUILD\'s ID.') . '</em>';
	$page .= theme('table', $headers, $rows);
	
	return $page;
}

function builds_add_existing() {
	$form = array();
	$builds = array();
	$rs = db_query("SELECT apb.bid, apb.build_name
								 FROM {ap_build} apb LEFT JOIN {ap_builds} apbs ON apb.bid = apbs.bid
								 WHERE (apbs.apid != %d || apbs.apid IS NULL)",
								 AutoPilot::setCurrentAutoPilot());
	while($row = db_fetch_object($rs)) {
		$builds[$row->bid] = $row->build_name;
	}
	
	if( count($builds) == 0) {
		$no_builds_help = t('You currenlty do not have any "builds".
			Please create a build first. Once that build is created, it
			will show up in this box to allow you to use it. If you
			do not see the "Add New Build" section and/or you do not
			have permission to create new builds, please contact your
			administrator to create a build for you to use.');
		$form['help'] = array(
			'#value' => $no_builds_help,
		);
		return $form;
	}
	$form['bid'] = array(
		'#type' => 'select',
		'#title' => t('Select a Build to add to this AutoPilot'),
		'#description' => t('Select a Build to add to this AutoPilot'),
		'#options' => $builds,
	);
	$form['add'] = array(
		'#type' => 'submit',
		'#value' => t('Add Build to AutoPilot'),
	);
	return $form;
}

function builds_add_existing_submit($form_id, $form_values) {
	$build = Build::buildFactory($form_values['bid']);
	if($build->addToAutoPilot()) {
		drupal_set_message(t('%name has been added to your AutoPilot',
												 array('%name' => $build->getName())));
	}
	else {
		drupal_set_message(t('There was a problem adding %name',
												 array('%name' => $build->getName())),
											 'error'
											 );
	}
	return;
}

/**
 * Form to add a Repo/Build to make it availiable to the AutoPilot
 */
function builds_add_build_form($bid = null) {
	_autopilot_init_js();
	$repos = _vc_get_repos();
	$sel[] = '[' . t('Select a Build') . ']';
	$js_sel[0] = '[' . t('Select a repository to see the full path displayed here.') . ']';
	foreach($repos as $k => $repo) {
		$sel[$repo->rpid] = $repo->rp_name . ':' . substr($repo->rp_path,0,30) . '...';
		$js_sel[$repo->rpid] = $repo->rp_path;
	}
	$js_info = array(
		'autopilot' =>
			array('build_list' => $js_sel),
	);
	drupal_add_js($js_info, 'setting', 'header', FALSE, FALSE);
	
	$build = Build::buildFactory($bid);
	if($build) {
		$build_path = $build->getRepoPath();
		$name = $build->getName();
		$rpid = $build->getRepoID();
		$ap_type = $build->getAPType();
	}

	$form['rpid'] = array(
		'#type' => 'select',
		'#title' => t('Select a repository to add as a Build'),
		'#description' => t('Description'),
		'#options' => $sel,
		'#default_value' => $rpid,
		'#suffix' => '<div id="repo_full_path_display">[' . t('Select a repository to see the full path displayed here.') . ']</div>',
		'#attributes' => array('onChange' => 'showFullBuildPath(this.selectedIndex)'),
	);
	
	$form['build_path'] = array(
		'#type' => 'textfield',
		'#title' => t('Path'),
		'#description' => t('Use this if there is a certain path off of the main
												repository that you would like to use.'),
		'#default_value' => $build_path,
	);
	$form['build_name'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#description' => t('A short title to describe this Build. If this is left
												blank, then the title of the repository used will
												be set as the title of this Build.'),
		'#default_value' => $name,
	);
	$ap_types = array('0' => '[SELECT ONE]');
	$ap_types += AutoPilot::getAPTypes();
	$form['ap_type'] = array(
		'#type' => 'select',
		'#title' => t('Build Type'),
		'#required' => true,
		'#options' => $ap_types,
		'#default_value' => $ap_type,
	);
	
	if($build) {
		$form['update_build'] = array(
			'#type' => 'submit',
			'#value' => t('Update Build'));
		$form['bid'] = array(
			'#type' => 'hidden',
			'#value' => $bid,
		);
	}
	else {
		$form['add_build'] = array(
			'#type' => 'submit',
			'#value' => t('Add Build'));
	}
	return $form;
}

function builds_add_build_form_validate($form_id, $form_values) {
	if($form_values['ap_type'] == 0 && is_numeric($form_values['ap_type'])) {
		form_set_error('ap_type', t('You must select the type of build this is.'));
	}
}

/**
 * Submission of a build to add
 */
function builds_add_build_form_submit($form_id, $form_values) {
	$repo = _vc_load_repo($form_values['rpid']);
	$classname = Build::buildFactory(null, $repo->rp_handler);
	if($classname != null) {
    watchdog('autopilot', t('Unable to fetch repository build class.'), WATCHDOG_ERROR);
    drupal_set_message(t('Error during submission. Unable to fetch repository build class.'), 'error');
    return FALSE;
  }
	$build = new $classname($form_values['bid']);
	if(!isset($form_values['bid'])) {
		$build->createNew();
	}
	$build->setRepoRef($form_values['rpid']);
	$build->setName($form_values['build_name']);
	$build->setBuildPath($form_values['build_path']);
	$build->setAPType($form_values['ap_type']);
	try {
		$ok = $build->save(); // Saves this build
	}
	catch(Exception $e) {
		drupal_set_message(t('Exception thrown: %e', array('%e' => $e->getMessage())));
		$ok = false;
	}
	
	if($ok) {
		drupal_set_message(t('The Build %build_name has been saved.',
											 array('%build_name' => $form_values['build_name'])));
	}
	else {
		drupal_set_message(t('There was an error while trying to save your build.'), 'error');
	}
	
	return;
}
