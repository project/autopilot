<?php
/**
 *
 * This file handles the interactions of a Build Plan.
 * It really manipulates the Build Plan object. This is for a number
 * of reasons. But was are thinking of porting this to a java class
 * and thus starting with an OO arch, pehraps not totally correctly
 * implemented, will make that transition smoother if the time comes.
 * 
 */


/**
 * Loads a Build Plan object by creating a new
 * buildplan object, and giving the bpid as
 * the var to the constructor.
 */
function buildplan_load($bpid) {
  $bp = new BuildPlan($bpid);
  return $bp;
}

/**
 * @abstract This is the main Build Plan page.
 * If we haven't selected a Build Plan to work
 * with, then we will just list the current
 * build plans for the selected AutoPilot.
 */
function buildplan_overview_page($bpid = null, $view = 'list') {
  $page = '';
  global $user;
  
  $build = new SimpleBuildPlan($bpid);

  $page .= "<h2>" . t('Build Plans') . "</h2>";
  
  if($bpid == 'add' || $bpid == 'edit') {
    $page .= drupal_get_form('buildplan_edit_form', $build);
    return $page;
  }
  else if ( is_numeric($bpid) ) {
    return buildplan_view_page($bpid);
  }
  else if( $view == 'delete' ) {
    return drupal_get_form('buildplan_delete_form', $bpid);
  }
  
  // List the current Build Plans
  // TODO Limit to where the uid/ownership is the current user.
  $autopilot = AutoPilot::setCurrentAutoPilot();
  if($autopilot == null) {
    drupal_set_message(t('You have not selected an Autopilot to work with.'));
    drupal_goto('autopilot');
  }
  $bprs = db_query("SELECT apbp.* FROM {ap_build_plan} apbp INNER JOIN {ap_build_plans} apbps ON apbp.bpid = apbps.bpid 
                   WHERE apbps.apid = %d", $autopilot->getID());
  $rows = array();
  $row = array();
  $header = array(t('Name'), t('Type'), t('Status'), t('Owner'), '-');
  while($crow = db_fetch_object($bprs)) {
    $row = array();
    $row[] = $crow->bp_name;
    $row[] = t('Simple Build Plan');
    $row[] = t('Active');
    $row[] = t('ownership');
    
    $links = array();
    $links[] = array('href' => 'autopilot/buildplans/' . $crow->bpid, 'title' => t('view'));
    $links[] = array('href' => 'autopilot/setup/buildplans/' . $crow->bpid . '/edit', 'title' => t('edit'));
    $links[] = array('href' => 'autopilot/setup/buildplans/' . $crow->bpid . '/delete', 'title' => t('delete'));
    $row[] = theme('links', $links);
    $rows[] = $row;
  }
  if(count($rows) > 0) {
    $page .= theme('table', $header, $rows, t('Your Build Plans'));
  }
  else {
    # Instructions on how to create a build plan.
    $help = '<p>You currently have no build plans.<br />
    To create a Build Plan, AT LEAST one (1) "build", and you must create two (2) targets:<br />
    </p>
    <h2>Creating the Build</h2>
    The <strong>Build</strong> is where you code repository is located.<br/>
    It is recommended that you created two (2) builds. <br/>
    <ul>
      <li>One build where your drupal code is located.</li>
      <li>One build where your sql files for your drupal install is located.</li>
    </ul>
    <h2>Targets</h2>
    1. Code Target - This is the location where your Drupal code will be served from. This
    is also known as a Web/HTTP target.
    <br />
    2. Database Target - This is where your MySQL/Postrges/Database is located.
    This is the location where Autopilot will load your SQL files into.
    </p>';
    $page .= $help;
    
  }
  
  // Links
  $links = array();
  // Only show the "Create Build" link if we have targets and builds
  $links[] = array('href' => 'autopilot/setup/buildplans/add',
                   'title' => t('Create new Build Plan'));
  $links[] = array('href' => 'autopilot/setup/build/new',
                   'title' => t('Create new Build'));
  $links[] = array('href' => 'autopilot/setup/targets/add',
                   'title' => t('Create new Target'));
  $links[] = array('href' => 'autopilot/setup/featuresets/new',
                   'title' => t('Create new Feature Set'));
  $page .= theme('links', $links);
  
  return $page;
}

/**
 * Main page to do "Maintianance" tasks
 */
function buildplan_maintianance($bpid) {
  // Grap the maintianance form
  $page = '';
  $page .= '<h3>Maintianance Tasks</h3>';
  $page .= drupal_get_form('buildplan_maintianance_form', $bpid);
  return $page;
}

function buildplan_maintianance_form($bpid) {
  $form = array();
  
  return $form;
}

/**
 * An overview page for a buildplans build runs
 */
function buildrun_log($bpid) {
  $page = '';
  $bpid = arg(2);
  $buildplan = new SimpleBuildPlan($bpid);
  $page .= '<h2>Run History for: ' . $buildplan->getName() . '</h2>';
  
  if($run_id != '') {
    
  }
  // If we don't have a runid, then we need to list the past runs
  else {
    $bpid = arg(2);
    $header = array(t('Run ID'), t('Run date'), t('Run End'), t('Run When'), t('Run Time'), t('Status'), '');
    $rs = db_query("SELECT * FROM {ap_buildrun} WHERE bpid = %d ORDER BY ap_buildrun_id DESC", $buildplan->getID());
    $status_map = BuildRun::getStatusMap();
    while($run = db_fetch_object($rs)) {
      $row = array();
      $row[] = $run->ap_buildrun_id;
      $row[] = format_date($run->ap_run_start, 'custom', 'M.d.Y');
      $row[] = format_date($run->ap_run_stop, 'custom', 'M.d.Y');
      
      $opt = array();
      $opt['precision'] = 'second';
      $opt['parts'] = 2;
      $opt['distance'] = true;
      $row[] = _autopilot_time_diff($run->ap_run_stop, $opt);
      
      $opt = array();
      $opt['to'] = $run->ap_run_start;
      $opt['precision'] = 'second';
      $opt['parts'] = 2;
      $opt['distance'] = false;
      $row[] = _autopilot_time_diff($run->ap_run_stop, $opt);
      $row[] = t($status_map[$run->ap_run_status]);
      
      $links = array();
      $links[] = array('title' => t('details'), 'href' => 'autopilot/buildplans/' . $bpid. '/buildrun_log/' . $run->ap_buildrun_id);
      $row[] = theme('links', $links);
      $rows[] = $row;
    }
    $page .= theme('table', $header, $rows);
  }
  
  return $page;
}

function buildrun_log_view($run_id) {
  $page = '';
  $run = new BuildRun($run_id);
  $form = array();
  
  // General Information
  $run_info = '';
  $run_info .= '<p>Run Name: ' . $run->getName() . '<p>';
  //$run_info .= '<p>Run Description: ' . '<em>just tought of this, need to implement.</em>' . '</p>';
  $run_info .= '<p>Start: ' . format_date($run->getRunStart(), 'large') . '</p>';
  $run_info .= '<p>Stop: '  . format_date($run->getRunStop(), 'large') . '</p>';
  $run_info .= '<p>Run time: ' . $run->getTotalTime() . '</p>';
  $run_info .= '<p>Run status: ' . BuildRun::buildRunStatus($run->getRunStatus()) . '</p>';
  
  $form['run_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Run Information'),
    '#value' => $run_info);
  $page .= drupal_render($form);
  
  // Logs table
  $form = array();
  $header = array(t('Logger'), t('Message'));
  $rows = array();
  $row = array();
  foreach($run->getLogs() as $k => $log) {
    $row = array();
    $row[] = $log['logger'];
    $row[] = $log['message'];
    $rows[] = $row;
  }
  $log_table = theme('table', $header, $rows, array(), t('Log Messages'));
  $form['logs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Log Messages'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#value' => $log_table);
  $page .= drupal_render($form);
  
  return $page;
}

/**
 * The view for an actual build plan
 */
function buildplan_view_page($bpid = null) {
  $page = '';
  $controlbox =  menu_get_local_tasks();
  
  $buildplan = new SimpleBuildPlan($bpid);
  
  // Build Info
  $page .= l(t('View Build Logs for this build plan'), "autopilot/buildplans/$bpid/buildrun_log");
  
  $page .= '<p>Name: ' . $buildplan->getName() . '</p>';
  
  if( arg(4) == 'edit') {
    $page .= drupal_get_form('buildplan_edit_form', $buildplan->getID());
    return $page;
  }
  else if( arg(4) == 'delete' ) {
    $page .= drupal_get_form('buildplan_delete_form', $buildplan->getID());
    return $page;
  }
  
  // List the steps in this build plan
  //$page .= drupal_get_form('buildplan_plan_table_form', $bpid);
  if( is_a('BuildPlan', $bpid) ){
    $buildplan = $bpid;
  }
  else {
    $buildplan = new SimpleBuildPlan($bpid);
  }
  
  // Plan Information
  $rows = array();
  $header = array('', '');
  $wt = $buildplan->getCodeTarget();
  $rows[] = array('<strong>' . t('Web Target Destination') . ' [' . $wt->getAddress() . ']' .  '</strong>',
                  $wt->getWebRootPath());
  $st = $buildplan->getSQLTarget();
  $sb = $buildplan->getSQLBuild();
  $rows[] = array('<strong>' . t('SQL Target Destination') . ' [' . $st->getAddress() . ']' .  '</strong>',
                  $sb->getRepoPath() . '<br />' . $st->getSQLDatabaseName());
  $page .= theme('table', $header, $rows);
  
  // Plan Repo Selection
  $page .= drupal_get_form('buildplan_select_repo_run_form', $buildplan->getID());
  
  return $page;
}

/**
 * Form that allows a user to select a repository
 */
function buildplan_select_repo_run_form($bpid) {
  _buildplan_init_js();
      
  $info = db_fetch_object(db_query("SELECT * FROM {ap_build_plan} WHERE bpid = %d", $bpid));
  $form = array();
  $form['run_build'] = array(
    '#type' => 'submit',
    '#prefix' => '<div id="run_build_submit">',
    '#id' => 'run_build_button',
    '#suffix' => '</div>',
    '#value' => t('Run Build Plan'),
  );
  $form['queue_build'] = array(
    '#type' => 'submit',
    '#prefix' => '<div id="queue_build_submit">',
    '#id' => 'queue_build_button',
    '#suffix' => '</div>',
    '#value' => t('Queue Build Plan'),
  );
  $form['repo_sel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Repositry selection'),
    '#collapsible' => true,
    '#collapsed' => true,
  );
  
  $revision_options = array();
  
  $cb = Build::buildFactory($info->code_build_bid);
  $revisions = $cb->getRevisions('full');
  $revision_options = array();
  $code_repo_path = $cb->getRepoPath();
  usort($revisions, '_autopilot_revision_sort');
  foreach ($revisions as $revision) {
    foreach($revision['paths'] as $path => $action) {
      if( stristr(trim($path), $code_repo_path) ) {
        $revision_options[ $revision['rev'] ] = $revision['rev'] . ": " . substr($revision['log'], 0, 80);
        continue;
      }
    }
  }
  $revision_options = array_reverse($revision_options, true);
  
  $form['repo_sel']['code_rev'] = array(
    '#type' => 'select',
    '#id' => 'code_revision',
    '#title' => t('Select a code revision'),
    '#options' => $revision_options,
  );
  
  $sb = Build::buildFactory($info->sql_build_bid);
  $revisions = $sb->getRevisions('full');
  $revision_options = array();
  $sql_repo_path = trim($sb->getRepoPath()); // So we're not calling a function alot in the loop
  
  foreach ($revisions as $revision) {
    // ONLY ADD IF THE PATH MATCHES EXACTLY
    foreach($revision['paths'] as $path => $action) {
      if( stristr(trim($path), $sql_repo_path) ) {
        $revision_options[ $revision['rev'] ] = $revision['rev'] . ": " . substr($revision['log'], 0, 80);
        continue;
      }
    }
  }
  $revision_options = array_reverse($revision_options, true);
  $form['repo_sel']['sql_rev'] = array(
    '#type' => 'select',
    '#id' => 'sql_revision',
    '#title' => t('Select a sql revision'),
    '#options' => $revision_options,
  );
  
  $form['bpid'] = array(
    '#type' => 'hidden',
    '#value' => $bpid);
  
  
  $form['repo_sel']['lock_builds'] = array(
    '#type' => 'checkbox',
    '#title' => t('Lock Builds'),
    '#id' => 'lock-builds',
    '#default_value' => true,
    '#description' => t('Locks your code and sql revisions togather. Please
                        click here to learn more about this feature.'),
  );
  $form['repo_sel']['lock_step_build'] = array(
    '#type' => 'checkbox',
    '#title' => t('Lock-Step Build'),
    '#id' => 'lock-step-build',
    '#default_value' => true,
    '#description' => t('By checking this box, the builds will be "Lock Stepped".
                        This means that the database on the target will be dumped and
                        committed to the database before the build takes place.'),
  );
  $form['repo_sel']['update_scope'] = array(
    '#type' => 'radios',
    '#title' => t('Update Scope'),
    '#id' => 'update-scope',
    '#options' => array('full' => t('Both'), 'code' => t('Code-Only')),
    '#default_value' => 'full',
  );
  $form['repo_sel']['backup_build'] = array(
    '#type' => 'checkbox',
    '#title' => t('Backup Current Build'),
    '#description' => t('When this is checked, when you start a Build Plan, AutoPilot will
                        backup the current build first BEFORE running your Build Plan.'),
    '#default_value' => true,
  );
  
  // Database Options
  $form['db_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Database Options'),
    '#collapsible' => true,
    '#collapsed' => true,
  );

  $form['db_options']['backup_sql'] = array(
    '#type' => 'submit',
    '#value' => t('Backup Database to Repository'),
  );
  

  // Feature sets
  if(module_exists('featureset')) {
    // General Options
    $form['cache_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Cache options'),
      '#collapsible' => true,
      '#collapsed' => true,
    );
  
    $form['cache_options']['rebuild_module_cache'] = array(
      '#type' => 'checkbox',
      '#title' => t('Rebuild Module Cache'),
      '#description' => t('Post-build, AutoPilot will rebuild the module and theme cache on the build. This is usually a good idea to have checked as the paths to the module files may be different per enviroment.'),
      '#default_value' => true,
    );
  
  
    $form += featureset_buildplan_form($bpid);
  }
  
  // Get 
  return $form;
}
/**
 *
 * Validation rules for the form
 *
 */
function buildplan_select_repo_run_form_validate($form_id, $form_values) {
  return;
}


function buildplan_select_repo_run_form_submit($form_id, $form_values) {
  global $stop;
  global $user;
  $bpid = $form_values['bpid'];
  $op = $form_values['op'];
  if($op == t('Queue Build Plan')) {
    if(buildplan_queue_plan($plan_info)) {
      drupal_set_message(t('Your plan has been sucessfully queued.'));
    }
    else {
      drupal_set_message(t('There was an error queing your build plan.'), 'error'); 
    }
    
  }
  else {
    _buildplan_run_plan($form_values);
  }
  
  // TODO: This has been moved to the ad-hoc function: _buildplan_run_plan($plan_settings);
  // Give us time

  return;
}


/**
 * BuildPlan Plan Table form
 */
function buildplan_plan_table_form($bpid) {
  // Setup
  $form = array();
  # BuildPlan Object
  if( is_a('SimpleBuildPlan', $bpid) ){
    $buildplan = $bpid;
  }
  else {
    $buildplan = new SimpleBuildPlan($bpid);
  }
  
  $header = array(t('Step'), t('Name'), '-');
  $rows = array();
  $row = array();
  $rs = db_query("
    SELECT * FROM ap_build_plan_steps WHERE bpid = %d
                 ", $buildplan->getID());
  while($step = db_fetch_array($rs)) {
    // Its a build
    if($step['bid']) {
      $build = null;
      $build = new Build($step['bid']);
      $row[] = $step['b.step'] ;
      $row[] = $build->getName();
    }
    // target
    else if($step['tid']) {
    }
    // commands
    else if($step['cid']) {
    }
  }
  
}

/**
 * Run the actual build process
 */
function buildplan_run_build_form_submit($form_id, $form_values) {
  $buildplan = new BuildPlan($form_values['bpid']);
  
  // Run the build
  $buildplan->runBuildPlan();
  
  drupal_set_message(t('Build plan has been run.'));
  return;
}

function buildplan_run_build_form($bpid) {
  $form = array();

  $form['bpid'] = array(
    '#type' => 'hidden',
    '#value' => $bpid,
  );
  
  return $form;
}


function _buildplan_get_types() {
  return array('build' => t('Build'),
               'target' => t('Target'),
               'command' => t('Command'),
              );
}

function _buildplan_get_type_refs($type) {
  global $user;
  $list = array();
  switch($type) {
    case 'build':
      $rs = db_query("SELECT bid as id, build_name as name, ap_type FROM {ap_build}");
      while($row = db_fetch_object($rs)) {
        $list[] = $row;
      }
      break;
    case 'target':
      $rs = db_query("SELECT tid as id, target_name as name FROM {ap_target} WHERE uid = %d", $user->uid);
      while($row = db_fetch_object($rs)) {
        $list[] = $row;
      }
      break;
    case 'command':
      $rs = db_query("SELECT cid as id, cmd_name as name FROM {ap_command} WHERE uid = %d", $user->uid);
      while($row = db_fetch_object($rs)) {
        $list[] = $row;
      }
      break;
  }
  return $list;
}

/**
 * Basic Build Plan forms
 */
function buildplan_edit_form($buildplan) {
  if(is_numeric($buildplan)) {
    $buildplan = new SimpleBuildPlan($buildplan);
  }
  
  $dbinfo = db_fetch_object(db_query("SELECT * FROM {ap_build_plan} WHERE bpid = %d", $buildplan->getID()));
  $form['buildplan_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Build Plan Title'),
    '#description' => t('A short title for this build plan.'),
    '#default_value' => $buildplan->getTitle(),
    '#required' => true,
  );
  
  $form['buildplan_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A more detailed description of this Build Plan.'),
    '#default_value' => $buildplan->getDescription(),
    '#required' => true,
  );
  $raw_labels = _autopilot_get_labels();
  $labels = array('0' => '[' . t('Select One') . ']');
  foreach($raw_labels as $k => $label) {
    $labels[$label->lid] = $label->title;
  }
  $form['lid'] = array(
    '#type' => 'select',
    '#title' => t('BuildPlan Label/Type'),
    '#description' => t('The type of BuildPlan this is. These selections are made by the administrator and allow you to label your BuildPlan.'),
    '#default_value' => $buildplan->getLabelID(),
    '#required' => true,
    '#options' => $labels,
  );
  $form['buildplan_ownership'] = array(
    '#type' => 'select',
    '#title' => t('Ownership'),
    '#description' => t('Ownership/Type'),
    '#default_value' => $buildplan->getOwnership(),
    '#options' => _autopilot_get_owernships('select'),
  );
  
  $builds = array();
  $targets = array();
  $cbuilds = array(); // Code/HTML Builds
  $sbuilds = array(); // SQL/DB Builds
  $wtargets = array(); // Web Targets
  $stargets = array(); // SQL/DB Targets
  /**
   * TODO:
   * The 'correct' way is to load each object and do a
   * 'instanceof' on them. However, that is kind of expensive.
   * So for now I am going to do the cheap way to checking the
   * type in the database.
   */
  $keychains[] = '[' . t('Use Item Default') . ']';
  
  $bt = _buildplan_get_type_refs('build');
  foreach($bt as $k => $build) {
    if($build->ap_type == 'CODE') {
      $cbuilds[$build->id] = $build->name;
    }
    else {
      $sbuilds[$build->id] = $build->name;
    }
  }
  
  $kc = _ponto_keychain_get_keychains(PONTO_KEYCHAIN_TYPE_SYSTEM);
  foreach($kc as $kcid => $title) {
    $keychains[$kcid] = $title;
  }
  
  $tc = Target::getAvailiableTargets();
  
  foreach($tc as $k => $utc) {
    if($utc->target_type == 'TargetWeb') {
      $wtargets[$utc->tid] = $utc->target_name;
    }
    else {
      $stargets[$utc->tid] = $utc->target_name;
    }
    
  }
  /** Begin the Build Information Section **/
  $form['build_info']['#theme'] = 'build_info_form_section';
  $form['build_info']['build_sec'] = array(
    '#type' => 'fieldset',
    '#title' => t('Build Plan'),
    '#collapsible' => true,
    '#collapsed' => true);
  /**
   * Web Components Row
   */
  # Code Build
  $form['build_info']['web']['repo']['item']['code_bid'] = array(
    '#type' => 'select',
    '#title' => t('Select a code build'),
    '#options' => $cbuilds,
    '#default_value' => $dbinfo->code_build_bid,
  );
  $form['build_info']['web']['repo']['auth']['code_build_kcid'] = array(
    '#type' => 'select',
    '#title' => t('Code Build Keychain'),
    '#options' => $keychains,
    '#default_value' => $dbinfo->code_build_kcid,
    
  );
  # Code Target
  $form['build_info']['web']['target']['item']['code_tid'] = array(
    '#title' => t('Code target'),
    '#type' => 'select',
    '#options' => $wtargets,
    '#default_value' => $dbinfo->code_target_tid,
  );
  $form['build_info']['web']['target']['auth']['code_target_kcid'] = array(
    '#type' => 'select',
    '#title' => t('Code Target Keychain'),
    '#options' => $keychains,
    '#default_value' => $dbinfo->code_target_kcid,
  );
  /**
   * Database/SQL Components Row
   */
  # SQL Build
  $form['build_info']['db']['repo']['item']['sql_bid'] = array(
    '#type' => 'select',
    '#title' => t('Select a SQL build'),
    '#options' => $sbuilds,
    '#default_value' => $dbinfo->sql_build_bid,
  );
  $form['build_info']['db']['repo']['auth']['sql_build_kcid'] = array(
    '#type' => 'select',
    '#title' => t('SQL Build Keychain'),
    '#options' => $keychains,
    '#default_value' => $dbinfo->sql_build_kcid,
  );
  # SQL Target
  $form['build_info']['db']['target']['item']['sql_tid'] = array(
    '#title' => t('SQL target'),
    '#type' => 'select',
    '#options' => $stargets,
    '#default_value' => $dbinfo->sql_target_tid,
  );
  $form['build_info']['db']['target']['auth']['sql_target_kcid'] = array(
    '#type' => 'select',
    '#title' => t('SQL Target Keychain'),
    '#options' => $keychains,
    '#default_value' => $dbinfo->sql_target_kcid,
  );
  /** END THE BUILD INFORMATION SECTION **/
  
  // To save of update
  if($buildplan->getID() != '') {
    $form['bpid'] = array('#type' => 'hidden', '#value' => $buildplan->getID());
    $form['buildplan_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update BuildPlan'),
    );
  }
  else {
    $form['buildplan_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create BuildPlan'),
    );
  }
  return $form;
}

function buildplan_edit_form_validate($form_id, $form_values) {
  if($form_values['lid'] == 0)  {
    form_set_error('lid', t('You must select a label.'));
    return;
  }
  if(is_numeric($form_values['bpid'])) {
    $bp = new SimpleBuildPlan($form_values['bpid']);
  }
  else {
    $bp = new SimpleBuildPlan();
    $bp->createNew();
  }
  
  $bp->setTitle($form_values['buildplan_title']);
  $bp->setDescription($form_values['buildplan_description']);
  $bp->setOwnership($form_values['buildplan_ownership']);
  
  # Set the build information items
  // Load the code build
  // TODO: Implement KeyChain use for SVN/Repo use
  
  $cbuild = Build::buildFactory($form_values['code_bid']);
  if(is_numeric($form_values['code_build_kcid'])) {
  }
  // SQL Build
  $sbuild = Build::buildFactory($form_values['sql_bid']);
  if(is_numeric($form_values['sql_build_kcid'])) {
  }
  // Code Target
  $ctarget = Target::targetFactory($form_values['code_tid']);
  if(is_numeric($form_values['code_target_kcid'])) {
    $ctarget->setKeyChain(new PontoKeyChain($form_values['code_target_kcid']));
  }
  // SQL Target
  $starget = Target::targetFactory($form_values['sql_tid']);
  if(is_numeric($form_values['sql_target_kcid'])) {
    $starget->setKeyChain(new PontoKeyChain($form_values['sql_target_kcid']));
  }
  
  // Start form error checks
  if(!is_a($cbuild, 'Build')) {
    form_set_error('code_bid', t('Invalid code build'));
  }
  if(!is_a($sbuild, 'Build')) {
    form_set_error('sql_bid', t('Invalid SQL Buid'));
  }
  if(!is_a($ctarget, 'Target')) {
    form_set_error('code_tid', t('Invalid Code Target'));
  }
  if(!is_a($starget, 'Target')) {
    form_set_error('sql_tid', t('Invalid SQL Target'));
  }
  
  return;
}

function buildplan_edit_form_submit($form_id, $form_values) {
  if(is_numeric($form_values['bpid'])) {
    $bp = new SimpleBuildPlan($form_values['bpid']);
  }
  else {
    $bp = new SimpleBuildPlan();
    $bp->createNew();
  }
  
  $bp->setTitle($form_values['buildplan_title']);
  $bp->setDescription($form_values['buildplan_description']);
  $bp->setOwnership($form_values['buildplan_ownership']);
  
  # Set the build information items
  // Load the code build
  // TODO: Implement KeyChain use for SVN/Repo use
  $cbuild = Build::buildFactory($form_values['code_bid']);
  if(is_numeric($form_values['code_build_kcid'])) {
  }
  // SQL Build
  $sbuild = Build::buildFactory($form_values['sql_bid']);
  if(is_numeric($form_values['sql_build_kcid'])) {
  }
  // Code Target
  $ctarget = Target::targetFactory($form_values['code_tid']);
  if(is_numeric($form_values['code_target_kcid'])) {
    $ctarget->setKeyChain(new PontoKeyChain($form_values['code_target_kcid']));
  }
  // SQL Target
  $starget = Target::targetFactory($form_values['sql_tid']);
  if(is_numeric($form_values['sql_target_kcid'])) {
    $starget->setKeyChain(new PontoKeyChain($form_values['sql_target_kcid']));
  }

  // Since this is a fast build, create a new BuildPlan object
  $cbuild->setLogger($bp->getLoggerName());
  $bp->setCodeBuild($cbuild);
  $sbuild->setLogger($bp->getLoggerName());
  $bp->setSQLBuild($sbuild);
  $ctarget->setLogger($bp->getLoggerName());
  $bp->setCodeTarget($ctarget);
  $starget->setLogger($bp->getLoggerName());
  $bp->setSQLTarget($starget);
  
  # Save
  try{
    $try = $bp->save();
  }
  catch(Exception $e) {
    drupal_set_message(t('Exception thrown: %e', array('%e' => $e->getMessage())));
    $try = false;
  }
  if($try) {
    drupal_set_message(t('The Build Plan %title was saved.',
                       array('%title' => $form_values['buildplan_title'])));
  }
  else {
    drupal_set_message(t('The Build Plan %title was NOT saved due to an error. Please try again or contact your administrator.',
                       array('%title' => $form_values['buildplan_title'])),
                       'error');
    return;
  }
  return 'autopilot/buildplans';
}

function buildplan_delete_form($bpid) {
  if($_POST['confirm']) {
    global $user;
    $bpid = $_POST['bpid'];
    db_query("DELETE FROM {ap_build_plan} WHERE bpid = %d AND uid = %d",
             $bpid, $user->uid);
    drupal_set_message(t('The build plan as been deleted.'));
    drupal_goto('autopilot/buildplans');
    return;
  }
  else {
    $form['bpid'] = array('#type' => 'hidden', '#value' => $bpid);
    return confirm_form($form, t('Are you sure you wish to remove this build plan?'),
                        'autopilot/buildplans',
                        t('This will remove the Build Plan only, and NOT
                          the underlying Builds, Targets, etc.'),
                        t('Remove Build Plan')
                        );
  }
}

/********
 *
 * Helper Functions
 */
function _autopilot_revision_sort($a, $b) {
  return $a['rev'] > $b['rev'];
}

// Initialize the Javascript functions
function _buildplan_init_js() {
  
  // Here we go
  $settings = array();
  $ap_path = drupal_get_path('module', 'autopilot');
  $settings['build_running_image'] = theme('image', "$ap_path/images/build_running.gif");
  $settings['build_complete_image'] = theme('image', "$ap_path/images/build_complete.png");
  $settings['bpid'] = arg(2);
  $settings['base_jquery_path'] = url('autopilot/jquery',null,null,TRUE);
  
  $settings = array('autopilot' => $settings);
  
  $js_path = drupal_get_path('module', 'autopilot') . '/autopilot.js';
  // TODO Fix javascript problems.
  //drupal_add_js($js_path, 'module', 'header', false, false);
  //drupal_add_js($settings, 'setting');
  return;
}

/**
 * Theme functions
 */

/**
 * Build info section form themer.
 */
function theme_build_info_form_section($form_values) {
  $images_path = drupal_get_path('module', 'autopilot') . '/media/images';
  $header = array(' ', t('Repository'), ' ', t('Destination/Target'));
  $rows = array();
  $row = array();
  // Web/HTTP Row
  $elements = element_children($form_id);
  $row[] = theme('image', $images_path . '/web.png', t('Web'), t('Web/HTTP Information'), array('align' => 'middle')) . '<strong>' . t('Web/HTTP') . '</strong>';
  # Code
  $cell = "";
  $cell .= drupal_render($form_values['web']['repo']['item']);
  $cell .= drupal_render($form_values['web']['repo']['auth']);
  $row[] = $cell;
  # Filler
  $row[] = theme('image', $images_path . '/go-next.png');
  # Target
  $cell = "";
  $cell .= drupal_render($form_values['web']['target']['item']);
  $cell .= drupal_render($form_values['web']['target']['auth']);
  $row[] = $cell;
  $rows[] = $row; // Commit row
  
  // Database
  $row = array();
  $row[] = theme('image', $images_path . '/network-server.png',  t('Database'), t('Database Information'), array('align' => 'middle')) . '<strong>' . t('Database') . '</strong>';
  # Code
  $cell = "";
  $cell .= drupal_render($form_values['db']['repo']['item']);
  $cell .= drupal_render($form_values['db']['repo']['auth']);
  $row[] = $cell;
  # Filler
  $row[] = theme('image', $images_path . '/go-next.png');
  # Target
  $cell = "";
  $cell .= drupal_render($form_values['db']['target']['item']);
  $cell .= drupal_render($form_values['db']['target']['auth']);
  $row[] = $cell;
  $rows[] = $row; // Commit row
  
  return theme('table', $header, $rows);
}


function _buildplan_run_plan($plan_settings) {
  set_time_limit(500);
  $bpid = $plan_settings['bpid'];
  
  // Setup
  $build_folder_name = 'build_' . time();
  $lock_step = $plan_settings['lock_step_build'];
  
  // Debug: Overriding for debugging, this
  // will use the default temp area.
  $tmp_area = variable_get('autopilot_tmp_path', file_directory_temp()) . '/' . $build_folder_name;
  $logger =& LoggerManager::getLogger($build_folder_name);
  $logger->info("Initializing build...");
  
  /**
   * Setup
   */
  # Since this is a fast build, create a new BuildPlan object
  $bp = new SimpleBuildPlan($bpid);
  $bp->setLogger($build_folder_name); // Set the logger
  
  # Set the feature sets
  if(is_array($plan_settings['avail_featuresets'])) {
    foreach($plan_settings['avail_featuresets'] as $fsid => $ok) {
      $bp->setEnabledFeatureSet($fsid);
    }
  }
  
  # Set the code revisions
  try {
    $logger->debug("Setting code revision " . $plan_settings['code_rev']);
    $bp->setCodeRevision($plan_settings['code_rev']);
    $logger->debug("Setting sql revision " . $plan_settings['sql_rev']);
    $bp->setSQLRevision($plan_settings['sql_rev']);
  }
  catch(Exception $e) {
    $logger->fatal("Fatal error during build");
    $logger->fatal($e->getMessage());
    drupal_set_message(t('Error during build'), 'error');
    drupal_set_message(t('BUILD ERROR') . ': ' . $e->getMessage(), 'error');
    return FALSE;
  }
  
  $logger->info("Initialization complete!");
  $update_scope = $plan_settings['update_scope'];
  
  /**
   * Sql Backup Section
   */
  if($op == t('Backup Database to Repository')) {
    # Setup
    $bp = new SimpleBuildPlan($bpid);
    $sql_build = $bp->getSQLBuild();
    $sql_target = $bp->getSQLTarget();
    $scratch_folder = 'sql_backup_' . time();
    
    // Need to make this dynamic
    $tmp_area = variable_get('autopilot_tmp_path', file_directory_temp());
    $os_info = _autopilot_detect_os();
    $base_path = $tmp_area . "/$scratch_folder";
    $file_name = 'site.sql';
    // TODO: Remove this debug line.
    print "Scratch: $scratch_folder. Base: $base_path" . "<br /><hr />";
    # Checkout to the local path
    if($base_path[strlen($base_path-1)] == '\\') {
      $checkout_path = substr($base_path,0, strlen($base_path));
    }
    else {
      $checkout_path = $base_path;
    }

    $sql_build->checkout('"' . $checkout_path . '"');

    # Lets grab the SQL from the target database
    @unlink($base_path . '/' . $file_name);
    # For Windows
    exec('DEL /F /Q "' . str_replace('/' , '\\', $base_path . $file_name) . '"');
    # Grab the backup from the remote DB Target
    $sql_target->backup($base_path . '/' . $file_name);
    drupal_set_message(t('The database for the target %name has been backed up.',
                       array('%name' => $sql_target->getName())));
    # Commit to the repo
    $msg = 'Committed by AutoPilot by ' . $user->name . ' on ' . format_date(time(), 'large');
    $sql_build->commit($base_path . '/' . $file_name, $plan_settings['backup_sql_log_msg'] . "\n - $msg");
    
    // Clean Up
    @unlink($base_path);
    exec('RMDIR /S /Q "' . str_replace('/' , '\\', $base_path) . '"'); // For windows
    
    // Update the cache if we are committing
    _svn_update_log_cache($sql_build->getRepoID());
    return;
  }
  
  // Run the build
  try {
    // Start a new BuildRun first
    $run = BuildRun::buildRunFactory($bp); // Encapsulate inside a run to catch throw errors
    $run->setLogger($build_folder_name);
    $run->setUpdateScope($update_scope);
    $run->setBuildName($build_folder_name);
    $run->setLockStep($lock_step);
    $run->setCacheRebuild('module', $plan_settings['rebuild_module_cache']);
    
    // TODO: Implement build backups
    if($plan_settings['backup_build'] && false) {
      drupal_set_message(t('Build was backed up'));
      $run->backupBuild();
    }
    $logger->info("Running build $build_folder_name.");
    
    $try = $run->runBuild();
    if($try) {
      $logger->info("Build $build_folder_name complete.");
      drupal_set_message(t('Fast Build %build_name Finished!', array('%build_name' => $run->getBuildName())));
    }
    else {
      drupal_set_message(t('Build %build_name seems to have thrown an error.
        Please check your build log for more information.', array('%build_name' => $run->getBuildName())),
        'error');
    }
  }
  catch ( Exception $e ) {
    $logger->fatal("Fatal error during build");
    $logger->fatal($e->getMessage());
    drupal_set_message(t('Error during build'), 'error');
    drupal_set_message(t('BUILD ERROR') . ': ' . $e->getMessage(), 'error');
    return FALSE;
  }
  #error_log("Build finished..");
  $logger->info("Build Finished");
  return TRUE;
}
