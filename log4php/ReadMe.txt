Place the Log4PHP files in this directory.

Be sure to run the following after installation:
ALTER TABLE autopilot_logger ADD COLUMN autopilot_log_id INT NOT NULL PRIMARY KEY auto_increment;
