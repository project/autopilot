<?php

function _autopilot_help($section) {
  $help = "";
  switch($section) {  
    case 'admin/help#autopilot':
      $help = '<p>' . t('Autopilot is a change management tool written by Workhabit Inc.') . '</p>';
      break;
    case 'autopilot':
      $help = '<p>' . t('This is your main dashboard. For more information click here') . '</p>';
      break;
    # Target Help
    case 'autopilot/setup/targets':
      $help = '<p>' . t('If you do not see any targets listed, you must create some targets.
                        If the %add_option is not availiable to you, then you must contact
                        your administrator to create some targets for you.',
                        array('%add_option' => t('Add Target'))
                        )
      . '</p>';
      break;
  }
  return $help;
}

function _autopilot_help_dashboard() {
  $page = "";
  return $page;
}
